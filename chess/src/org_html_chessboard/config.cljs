(ns org-html-chessboard.config)

(comment (str "Note: js/goog.DEBUG is true by default. In project.clj file, set "
              "goog.DEBUG in advanced (or min) compilation build "
              "     :cljsbuild {:builds [... {:id \"min\" ... :compiler {... :optimizations :advanced :closure-defines {goog.DEBUG false}}}]}"))

(def debug?
  ^boolean js/goog.DEBUG)

(when debug?
  ;;(devtools/install!)  ;; we love https://github.com/binaryage/cljs-devtools
  (enable-console-print!))
