(ns ^:figwheel-hooks org-html-chessboard.core
  (:require [org-html-chessboard.tools :as t :refer [rand-str log log-ind error alert]]
            
            [org-html-chessboard.search-bar :refer [run-injectSearchBar]]
            [org-html-chessboard.hide-outlines :as ho :refer [run]]
            [org-html-chessboard.config :as config]
            [org-html-chessboard.link-imgs :as li]
            [org-html-chessboard.chess-src-blocks :as csb]
            ;; Foreign libs (only to test that npm dependencies are bundled, see fn dev-setup).
            ;; moment.js is not registered in cljs foreign-libs, since it is not used (used only as example).
            ;;[moment] 
            [npm.chessjs]
            ;;[cljsjs.chess.js]
            [npm.chessboardjs]))

;; -- Debugging aids ----------------------------------------------------------
;;(devtools/install!)       ;; we love https://github.com/binaryage/cljs-devtools
;;(enable-console-print!)
;; See org-html-chessboard.config

(println "This text is printed from src/org-html-chessboard/core.cljs. Edit it and see reloading in action!")

(defn dev-setup []
  "Function to setup whatever it needs for development"
  (when config/debug?
    (println "Development mode is activated!")
    ;;(devtools/install!)
    (do
      (js/console.log "Trying out if npm external (non-cljsjs, non-closure  modules) deps are loaded. ")
      ;; moment.js
      #_(do  (js/console.log "Testing moment library.")
           (js/console.log  (str "Requires: moment.js (\"$ npm add moment\" and "
                                 "in this file \"(:require [moment])\")."))
           (js/console.log moment)
           (js/console.log (str "Hello there it's moment.js loaded with npm and webpack. Time of day: "
                                (.format (moment) "dddd"))))
      ;; chess.js
      (do (js/console.log "Testing chess.js library.")
          (js/console.log  (str "Requires: chess.js (\"$ npm add chess.js\" and "
                                "in this file \"(:require [npm.chessjs])\" and "
                                "in dev.cljs.edn: "
                                "{... :foreign-libs [{:file \".../chess.js\" :provides [\"npm.chessjs\"]}]})"))
          (let [c (js/Chess)]
            (.move c "e4")
            (js/console.log (.ascii c))))
      ;; chessBoard.js ;;TODO: Does it need jquery? It needs its own css (see
      ;; in node_modules) perhaps set in :asset-path?
      (do (js/console.log "Testing chessboard.js lib.")
          (js/console.log  (str "Requires: chessboard.js ("
                                "in this file \"(:require [npm.chessboardjs])\" and "
                                "in dev.cljs.edn ...)"))
          ;; Note: Chessboard needs to be given an existing html-node-id (else it will console.log an error).
          (let* [html-tag-id "small-board-start"
                 c (js/Chessboard html-tag-id "start")]
            (js/console.log (.fen c))))
      )))

;; define your app data so that it doesn't get over-written on reload
(defonce app-state (atom {:text "Hello world!"}))



(defn ^:export start-up []
  "This start-up fn has meta-data :export, which means, can be run in html (js) with: 
<script>org-html-chessboard.core.start_up();</script>"
  (dev-setup)
  #_(run-injectSearchBar)
  #_(ho/run)
  (li/run)
  (csb/run)
  )

(js/setTimeout start-up 1000)




;;----------------------------------------------------
;; Figwheel reload hooks
;; first notify figwheel that this ns has callbacks defined in it
;; with: (ns ^:figwheel-hooks ...)
;; See: https://figwheel.org/docs/hot_reloading.html
;;---------------------------------------------------

(defn ^:before-load my-before-reload-callback []
  (log (str "You can actually have :figwheel-hooks individually in each file. Here "
            "this function has to call the before-load fns of each namespaces."))
  (log "Before reload...")
  (csb/figwheel-before-load)
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
     )

(defn ^:after-load my-after-reload-callback []
  (log "After reload...")
  ;; We force a UI update by clearing the Reframe subscription cache.
  ;;(rf/clear-subscription-cache!)
  )



