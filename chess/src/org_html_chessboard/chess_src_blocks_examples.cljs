(ns org-html-chessboard.chess-src-blocks-examples
    (:require [clojure.string :as cljStr]
              [clojure.set :as cset]
              [goog.dom :as dom]
              [goog.dom.TagName :as TagName]
              [org-html-chessboard.tools :as t :refer [log log-ind error alert]]
              ;;chess.js
              [npm.chessjs]
              ;;[cljsjs.chess.js]
              [npm.chessboardjs])
    ;;(:import ["@chrisoakman/chessboardjs/dist/chessboard-1.0.0.js" Chessboard])
    )

(def example-5001-node-id "play-with-random")
(def board-5001 (atom nil))
(def engine-5001 (atom (js/Chess)))

(defn example-5001 []
  "Example: Play Random Computer  INCOMPLETE!

Requires that html has following node:
    <div id=\"play-with-random\" style=\"width: 400px\"></div>

This tries to emulate the example found at https://chessboardjs.com/examples#5001."
  (letfn [(onDragStart [src piece pos orientation] true
            (and (not (.game_over @engine-5001))
                 ;; Only pick movements of white
                 (do #_(println (str "*** PIECE: " piece))
                     (not (re-seq #"b" piece)))))
          (make-random-move []
            (let [move (first (.moves @engine-5001))]
              ;;TODO: pick random move
              (.position @board-5001
                         (.fen @engine-5001 (.move @engine-5001 move)))))
          (onDrop [src dest]
            (if-let [move (.move @engine-5001 #js {:from src
                                                   :to dest
                                                   :promotion "q"})]
              (do (.setTimeout js/window make-random-move 250))
              ;;Else:
              "snapback"))
          (onSnapEnd []
            (.position @board-5001 (.fen @engine-5001)))]
    (reset! board-5001  (js/Chessboard example-5001-node-id
                                      #js {:position "start"
                                           :draggable true
                                           :onDragStart onDragStart
                                           :onDrop onDrop
                                           :onSnapEnd onSnapEnd }))))


(comment "
// NOTE: this example uses the chess.js library:
// https://github.com/jhlywa/chess.js

var board = null
var game = new Chess()

function onDragStart (source, piece, position, orientation) {
  // do not pick up pieces if the game is over
  if (game.game_over()) return false

  // only pick up pieces for White
  if (piece.search(/^b/) !== -1) return false
}

function makeRandomMove () {
  var possibleMoves = game.moves()

  // game over
  if (possibleMoves.length === 0) return

  var randomIdx = Math.floor(Math.random() * possibleMoves.length)
  game.move(possibleMoves[randomIdx])
  board.position(game.fen())
}

function onDrop (source, target) {
  // see if the move is legal
  var move = game.move({
    from: source,
    to: target,
    promotion: 'q' // NOTE: always promote to a queen for example simplicity
  })

  // illegal move
  if (move === null) return 'snapback'

  // make random legal move for black
  window.setTimeout(makeRandomMove, 250)
}

// update the board position after the piece snap
// for castling, en passant, pawn promotion
function onSnapEnd () {
  board.position(game.fen())
}

var config = {
  draggable: true,
  position: 'start',
  onDragStart: onDragStart,
  onDrop: onDrop,
  onSnapEnd: onSnapEnd
}
board = Chessboard('myBoard', config)

")



(def example-spare-pieces-node-id "chess-free-board")
(def board-spare-pieces (atom nil))
(def engine-spare-pieces (atom (js/Chess)))

(defn example-spare-pieces []
  "Example: Free board.

Requires that html has following node:
    <div id=\"chess-free-board\" style=\"width: 400px\"></div>

This tries to emulate the example found at https://chessboardjs.com/examples."
  (letfn [(onDrop->update [src dest]
            (println (str "*** SRC: "  src))
            (println (str "*** DEST: " dest)))]
    (reset! board-spare-pieces  (js/Chessboard example-spare-pieces-node-id
                                      #js {:draggable true
                                           :sparePieces true
                                           :dropOffBoard "trash"
                                           ;; To update stuff:
                                           :onDrop onDrop->update}))))



(defn run []
  (example-5001)
  (example-spare-pieces))



