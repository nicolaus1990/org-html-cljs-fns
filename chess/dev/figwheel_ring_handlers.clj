(ns figwheel-ring-handlers
  (:require ;;These ring deps came (probably?) with figwheel.
            [ring.middleware.reload :refer [wrap-reload]]
            [ring.middleware.defaults :as rmid]
            [ring.util.response :refer [resource-response content-type not-found]]
            ;;[compojure.route :as route]
            [clojure.java.jdbc :as jdbc]
            [clojure.pprint :as pprint])
  (:gen-class))

;;---------------------------------------------------------------
;; Tools
;;---------------------------------------------------------------


;;-------------- Logging

(defn pformat [& args]
  ;;Pretty print but *out* is Stringbuffer and not stdout.
  (with-out-str
    (apply pprint/pprint args)))

(def log-debug (fn [& rest] (do (println "DEBUG:\n") (apply println rest))))
(def log-info (fn [& rest] (do (println "INFO:\n") (apply println rest))))

(defn log-middleware-only-request
  ;;Middleware to debug request
  ([handler] (log-middleware-only-request handler "RECEIVED: "))
  ([handler prefixReq]
   (fn [request]
     (log-debug (str prefixReq (pformat request)))
     (handler request))))

(defn log-middleware
  ;;Middleware to debug request and check response
  ([handler] (log-middleware handler "RECEIVED: " "ANSWERED: "))
  ([handler prefixReq prefixRes]
   (fn [request]
     (let [response (handler request)]
       (log-debug (str prefixReq (pformat request)))
       (log-debug (str prefixRes (pformat response)))
       response))))


;;---------------------------------------------------------------
;;; Figwheel ring-handlers
;;---------------------------------------------------------------

(def static-files-with-compojure
  (-> false;;(route/resources "/")
      ;;(wrap-content-type)
      ;;(wrap-not-modified)
      ))

;; the routes that we want to be resolved to index.html
(def route-set #{"/"})

(defn static-files [req]
  ;; Taken from: https://figwheel.org/docs/ring-handler.html
  (or
   (when (route-set (:uri req))
     (some-> (resource-response "index.html" {:root "public"})
             (content-type "text/html; charset=utf-8")))
   (not-found "Not found")))



(def build-handlers-dev static-files)
#_(def handler-for-figwheel-inner-dev (make-app))
;; Handler for development (figwheel)
(def handler-for-figwheel-dev
  ;; Explanation:
  ;; To make client/server tests, the backend (server) has to serve the files
  ;; (html/js/css files) to be tested. I.e: A client (started with figwheel's
  ;; custom server) will not be able to communicate with another separate
  ;; server (Same origin policy). Thus we need to specify to figwheel a
  ;; handler. In figwheel-main.edn:
  ;;     {:ring-handler n-server.web/handler-for-figwheel [...]}
  ;; Note: This is all optional. It is possible to use instead a default
  ;; figwheel internal handler.
  (do
    ;; Middleware wrap-reload reloads ns of modified files before the request
    ;; is passed to handler.
    ;; Wrap-reload requires that the fn has a name (to determine ns)
    ;; So, putting the handler in a let-form won't work, ie this will not
    ;; work:
    ;; (let [my-handlers (make-app)]
    ;;   (fn [req] (my-handlers req)))
    #_(wrap-reload #'handler-for-figwheel-inner-dev {:dirs ["src/clj"]})
    (wrap-reload #'build-handlers-dev {:dirs ["dev"]})))


(defn handler-for-figwheel-dev [req]
  ;;Simple test handler
  {:status 404
   :headers {"Content-Type" "text/html"}
   :body "Yep the server failed to find it."})
