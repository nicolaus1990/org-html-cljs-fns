(ns org-html-chessboard.chess-src-blocks-test
  (:require [cljs.test :refer-macros [deftest is testing run-tests]]
            [clojure.string :as cljStr]
            [clojure.set :as cset]
            ;;chess.js
            [npm.chessjs]
            ;;[cljsjs.chess.js]
            [npm.chessboardjs]
            [org-html-chessboard.chess-src-blocks :as csb]
            [org-html-chessboard.tools :as t :refer [log log-ind error alert]]))

(deftest chessjs-fen->chessboardjs-fen-test
  "Tests fn chessjs-fen->chessboardjs-fen"
  #_(is (= 1 4))
  (is (= "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR"
         (csb/chessjs-fen->chessboardjs-fen "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")))
  )

(deftest test-lib-chessjs
  "Proves that some chess.js behave as expected"

  (testing "Fen of chess.js "
    ;; Note that if an invalid fen is given, chessjs returns empty board:
    (let [chess-logic (js/Chess "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR")]
      (is (= "8/8/8/8/8/8/8/8 w - - 0 1" (.fen chess-logic))))
    ;;
    (let [fen "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"
          chess-logic (js/Chess fen)]
      (is (= fen (.fen chess-logic)))))
  
  (testing "Comments of chess.js "
    ;; chess.js also stores comments to a certain extent.
    (let [chess-logic (doto (js/Chess)
                        (.load_pgn "1. e4 e5 2. Nf3 Nc6 3. Bc4 Bc5 {giuoco piano} *"))]
      (is (= "giuoco piano" (.get_comment chess-logic)))))
  )


