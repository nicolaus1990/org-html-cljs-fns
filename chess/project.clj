(defproject org-html-chessboard "0.1.1"
  :description "FIXME: write this!"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}

  :min-lein-version "2.9.1"

  :dependencies [[org.clojure/clojure "1.10.0"]
                 ;; For DB-API
                 [org.clojure/java.jdbc "0.7.8"]
                 [org.postgresql/postgresql "42.2.5.jre7"]
                 ;;Cljs deps
                 [org.clojure/clojurescript "1.10.773"]
                 [org.clojure/core.async "0.4.500"]
                 [re-frame "1.0.0"]
                 [reagent "1.0.0-alpha2"]
                 ;;[re-frame "0.10.6" :exclusions [reagent]]
                 ;;[kee-frame "0.3.1"]
                 ;;[cljs-http "0.1.46"]
                 ;;[re-dnd "0.1.13"]
                 [re-com "2.8.0"]
                 ;; Deps cljsjs
                 ;; Find cljsjs libs in http://cljsjs.github.io/
                 ;; For list-app:
                 [cljsjs/react-sortable-hoc "1.11.0-1"] ;;https://github.com/clauderic/react-sortable-hoc
                 ;; For chess board 
                 ;;[cljsjs/chess.js "0.10.2-0"];; There is a newer version in npm of chess.js
                 [cljsjs/p5 "0.9.0-0"] ;; https://github.com/cljsjs/packages/tree/master/p5
                 ;;Don't move devcards deps to :dev dependencies (will break).
                 ;;Devcards is intended for development, but Google closure
                 ;;compiler will hopefully detect it as dead code and not
                 ;;include it in production code.
                 ;;[devcards "0.2.6"]
                 ]

  :plugins [ ;;[lein-npm "0.6.2" :exclusions [[org.clojure/clojure]]]
            [lein-simpleton "1.3.0"];; For serving files
            [lein-cljsbuild "1.1.7" :exclusions [[org.clojure/clojure]]]]

  :source-paths ["src"]

  :cljsbuild {:builds
              [{:id           "dev"
                :compiler     {:main                 org-html-chessboard.core
                               :asset-path           "js/compiled/out"
                               :output-to            "resources/public/js/compiled/org_html_chessboard.js" 
                               :output-dir           "resources/public/js/compiled/out"
                               :source-map-timestamp true
                               :optimizations :none
                               ;; The :preloads are only compatible with :optimizations :none
                               ;; To console.log CLJS data-structures make sure you enable devtools in Chrome
                               ;; https://github.com/binaryage/cljs-devtools
                               :preloads             [devtools.preload]
                               :foreign-libs [{:file "node_modules/chess.js/chess.js"
                                               :provides  ["npm.chessjs"]}
                                              {:file "node_modules/@chrisoakman/chessboardjs/dist/chessboard-1.0.0.js"
                                               :provides  ["npm.chessboardjs"]}]}}
               ;; This next build is a compressed minified build for
               ;; production. You can build this with:
               ;; lein cljsbuild once min
               {:id           "min"
                :source-paths ["src"]
                :compiler     {:main          org-html-chessboard.core
                               ;; Opt :asset-path is only needed for optimi :none/:whitespace
                               :asset-path           "js/compiled/out_prod"
                               :output-to            "resources/public/js/compiled/org_html_chessboard.js" 
                               :output-dir           "resources/public/js/compiled/out_prod"
                               :closure-defines {cljs.core/*global* "window"
                                                 goog.DEBUG false}
                               ;;:pretty-print  false ;;default

                               ;;:optimizations :none           ;;Works
                               ;;:optimizations :whitespace
                               :optimizations   :simple
                               ;;:optimizations :advanced
                               
                               ;; Externs, when advanced compilation, prevents
                               ;; munging of var names. (See:
                               ;; http://lukevanderhart.com/2011/09/30/using-javascript-and-clojurescript.html)
                               ;; :externs ["js_externs/externs_chess.js"]
                               :foreign-libs [{:file "node_modules/chess.js/chess.js"
                                               :provides  ["npm.chessjs"]}
                                              {:file "node_modules/@chrisoakman/chessboardjs/dist/chessboard-1.0.0.js"
                                               :provides  ["npm.chessboardjs"]}]
                               ;; For debugging, (particularly when trying to
                               ;; find out if an object of foreign-libs needs
                               ;; to be 'externed') uncommenting this could
                               ;; help:
                               ;; :pseudo-names true  :pretty-print true

                               ;; Using webpack is theoretically possible:
                               ;; :target :bundle
                               ;; :bundle-cmd {:none ["npx" "webpack" "out/index.js" "-o" "out/main.js" "--mode=development"]
                               ;;              :default ["npx" "webpack" "out/index.js" "-o" "out/main.js"]}
                               }}]}

  ;; need to add the compliled assets to the :clean-targets
  ;;:clean-targets ^{:protect false} ["resources/public/js/compiled" :target-path]
  ;;:clean-targets ^{:protect false} ["target"]
  :clean-targets ^{:protect false} [:target-path
                                    ;; These paths wont work, because map is
                                    ;; {:builds [{:id ...}]} instead of {:builds {:app {...}}}.
                                    ;;[:cljsbuild :builds :app :compiler :output-dir]
                                    ;;[:cljsbuild :builds :app :compiler :output-to]
                                    "resources/public/js/compiled/org_html_chessboard.js"
                                    "resources/public/js/compiled/out_dev"
                                    "resources/public/js/compiled/out_prod"]

  :profiles {:dev {:dependencies  [[binaryage/devtools "0.9.10"]
                                   ;; Instead of using lein-figwheel plugin, we use the
                                   ;; new figwheel-main: https://github.com/bhauman/figwheel-main
                                   ;; A simple config: https://gist.github.com/bhauman/a5251390d1b8db09f43c385fb505727d
                                   [com.bhauman/figwheel-main "0.2.11"]
                                   [com.bhauman/rebel-readline-cljs "0.1.4"]
                                   ;;[figwheel-sidecar "0.5.19"]
                                   ]
                   ;; need to add dev source path here to get figwheel handler and user.clj loaded
                   :source-paths  ["src"
                                   "dev"
                                   ;; We need to add cljs tests folder to add unit-tests (go
                                   ;; to http://localhost:9500/figwheel-extra-main/auto-testing).
                                   "tests"]
                   ;; setup target as a resource path for figwheel
                   :resource-paths ["target" "resources"]}}
  :aliases {;;----------------------------------------------------
            ;; Production (note: will still run with 'dev' profile, i.e.: log4j2 and other deps are included)
            "prod" ["with-profile" "production" "do" "clean," "fig:test," "bmin," "p3000"]
            "prod2" ["with-profile" "production" "do" "clean," "bmin," "p3000"]
            "prod-no-optimization" ["with-profile" "production" "do" "clean," "fig:test," "bdev," "p3000"]
            "p3000" ["simpleton" "3000" "file" ":from" ;; First arg is port.
                     "/home/nm/Documents/ProgrammingWorkspace/org-html-chessboard/resources/public/"] 
            ;;----------------------------------------------------
            ;; Build with cljsbuild
            "bdev" ["cljsbuild" "once" "dev"]
            "bmin" ["cljsbuild" "once" "min"]
            ;;----------------------------------------------------
            ;; npm (In command-line. See also: https://figwheel.org/docs/npm.html)
            ;; $ npm init -y                                ;; Will create package.json
            ;;
            ;; We need webpack to bundle our application along with our
            ;; production dependencies to make it available to the cljs-code.
            ;; Figwheel will run webpack (see dev.cljs-edn :target :bundle).
            ;;
            ;; $ npm add --save-dev webpack webpack-cli     ;; Add dev deps: webpack to bundle npm prod-deps
            ;; $ npm install @chrisoakman/chessboardjs      ;; Add prod deps
            ;; $ npm add @chrisoakman/chessboardjs          ;; Or npm add?
            ;;
            ;; For chessboard we also need its css. We will do this manually:
            ;; copy them and import styles in index.html: 
            ;; cp node_modules/@chrisoakman/chessboardjs/dist/chessboard-1.0.0.css resources/public/css/
            ;; or, the minified version:
            ;; cp node_modules/@chrisoakman/chessboardjs/dist/chessboard-1.0.0.min.css resources/public/css/
            ;; The npm chessboard package does not contain images of the
            ;; pieces. We can get some pieces with:
            ;; https://chessboardjs.com/releases/chessboardjs-1.0.0.zip
            ;;
            ;; For development mode, set in dev.cljs.edn :target, :bundle-cmd
            ;; (eventually :output-to and final-output-to), and :foreign-libs
            ;; to their appropiate values.
            ;;
            ;; TODO: Production and :advanced compilation ?

            ;;------------------------------------------------------------------
            ;;                     --- Figwheel ---
            ;; -----------------------------------------------------------------
            ;; --- Explanations ---
            ;; Figwheel configuration files: figwheel-main.edn, <PROFILE_NAME>.cljs.edn
            ;; Note: Figwheel will output a file in (this is default)
            ;; "target/public/cljs-out/dev-main.js" which is going to be available to
            ;; file "resource/public/index.html" (or wherever the static files are being
            ;; served).
            ;;
            ;; Lein profile "dev" contains also "tests" folder as source file, for
            ;; figwheel auto-testing (cljs hot-reload unit-tests).
            ;; -------------------------------------------------------------------------
            "fig"       ["trampoline" "run" "-m" "figwheel.main"]
            ;; -b dev or --build dev flag option -> figwheel reads dev.cljs.edn for build configuration
            ;; -r or --repl flag -> REPL should be launched
            ;; remove the string "trampoline" below if you are using Windows
            "fig:dev" ["trampoline" "run" "-m" "figwheel.main" "-b" "dev" "-r"]
            ;; Instead of visiting endpoint figwheel-extra-main/auto-testing
            ;; to check cljs unit-tests, use "fig:test" to run unit-tests once
            ;; in cml.
            "fig:test"  ["run" "-m" "figwheel.main" "-co" "test.cljs.edn"
                         "-m" "org-html-chessboard.test-runner"]
            ;; To debug figwheel config --print-config
            "fig-conf" ["trampoline" "run" "-m" "figwheel.main" "-pc" "-b" "dev" "-r"]
            })
