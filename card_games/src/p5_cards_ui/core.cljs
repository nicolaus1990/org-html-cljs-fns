(ns ^:figwheel-no-load p5-cards-ui.core
  (:require [p5-cards-ui.tools :as t :refer [rand-str log log-ind error alert]]
            [p5-cards-ui.config :as config]
            ;;[devtools.core :as devtools]
            [p5-cards-ui.p5-example2 :as p5-ex2]))

;; -- Debugging aids ----------------------------------------------------------
;;(devtools/install!)       ;; we love https://github.com/binaryage/cljs-devtools
;;(enable-console-print!)
;; See p5-cards-ui.config

(println "This text is printed from src/p5-cards-ui/core.cljs. Edit it and see reloading in action!")

(defn dev-setup []
  "Function to setup whatever it needs for development"
  (when config/debug?
    (println "Development mode is activated!")
    ;;(devtools/install!)
    (do
      (js/console.log "Trying out if npm external (non-cljsjs, non-closure  modules) deps are loaded. ")
      ;; moment.js
      #_(do  (js/console.log "Testing moment library.")
           (js/console.log  (str "Requires: moment.js (\"$ npm add moment\" and "
                                 "in this file \"(:require [moment])\")."))
           (js/console.log moment)
           (js/console.log (str "Hello there it's moment.js loaded with npm and webpack. Time of day: "
                                (.format (moment) "dddd"))))
      )))



(defn ^:export start-up []
  "This start-up fn has meta-data :export, which means, can be run in html (js) with: 
<script>p5-cards-ui.core.start_up();</script>"
  #_(dev-setup)
  #_(run-injectSearchBar)
  #_(ho/run)
  #_(li/run)
  ;;(csb/run)
  (p5-ex2/run)
  )

(js/setTimeout start-up 1000)








