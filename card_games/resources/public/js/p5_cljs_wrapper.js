function setup() {
    org-html-cljs-fns.p5-example1.setup();
}

function draw() {
    org-html-cljs-fns.p5-example1.draw();
}

function keyPressed() {
    if (keyCode !== 91) {
	org-html-cljs-fns.p5-example1.keyTyped(key, keyCode);
    }
}
