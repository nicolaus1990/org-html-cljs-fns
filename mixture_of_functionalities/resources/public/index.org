#+Title: Some org-file
# For css, js import see header "Export settings"

* Export settings                                          :ARCHIVE:noexport:
# Only include two levels in TOC:
#+OPTIONS: toc:2
# #+OPTIONS: toc:nil

# ---------------------------------------------------
# JS and CSS (external libs)
# ---------------------------------------------------

# Readtheorg setup
#+SETUPFILE: ./theme-readtheorg-local.setup

# Chessboard.js deps
#+SETUPFILE: ./deps-chessboardjs-local.setup

# Re-com deps
# #+SETUPFILE: ./deps-recom-local.setup


# ---------------------------------------------------
# My css and js (cljs) code
# ---------------------------------------------------

# My modifications:
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="css/style.css" />
#+HTML_HEAD: <link rel="icon" type="text/css" href="icons/favicon.ico" />
#+HTML_HEAD_EXTRA:  <script src="js/compiled/org_html_cljs_fns.js"></script>

# For figwheel: 
#+HTML_HEAD_EXTRA:  <script src="cljs-out/dev-main.js"></script>



* For cljs tests
Run ~lein fig:test~ (see aliases in project.clj file) or ~lein fig:dev~ and go
to : [[http://localhost:9500/figwheel-extra-main/auto-testing][figwheel-extra-main/auto-testing]]

* Headline num1                                                          :h2:
  :PROPERTIES:
  :keyOne:   valOne
  :END:

** Todo list
- Walk out Max.
- Vet Felix
- See what Anne is been up to.

** Favourite animals                                                   :favs:
*** Dog                                                                 :pet:
My dog's name is Max.
*** Cat                                                            :pet:wild:
My cat's name is Felix.
*** Squirrel                                                      :wild:best:
I know a squirrel named Anne.
*** Sloth                                                         :wild:best:
Sloths are great!
* Headline num2                                                          :h2:
** People with superpowers
*** Favourite villans                                                  :favs:
**** Joker                                                             :best:
**** Lex Luthor
**** Venom
*** Favourite superheroes                                              :favs:
**** Batman                                                            :best:
**** Superman
**** Spiderman
** Another structure
- A
  - B
  - C
    - D
- E
  - F
    - G
  - H
* Links
** Favourite CS links
*** [[https://www.youtube.com/channel/UCaLlzGqiPE2QRj6sSOawJRg][ClojureTV - YouTube (not xml)]]                                   :rssFeed:
*** [[https://www.youtube.com/channel/UC9m7D4XKPJqTPCLSBym3BCg][Fred Overflow - YouTube]]
** Libraries used in cljs code
- chessboard.js
  - [[https://chessboardjs.com/index.html][chessboardjs.com » Homepage]]
  - [[https://github.com/oakmac/chessboardjs/][oakmac/chessboardjs: JavaScript chessboard]] 
  - [[https://www.npmjs.com/package/@chrisoakman/chessboardjs][@chrisoakman/chessboardjs - npm]] 
- [[https://github.com/jhlywa/chess.js][jhlywa/chess.js: A Javascript chess library for chess move generation/validation, piece placement/movement, and check/checkmate/draw detection]] 
- From: [[http://cljsjs.github.io/][Javascript Libraries packaged for ClojureScript]]
  - [[http://clauderic.github.io/react-sortable-hoc/][React Sortable Higher-order Components]] 
- Dev deps
  - [[https://figwheel.org/][figwheel-main | Figwheel Main provides tooling for developing ClojureScript applications]] 
  - [[https://leiningen.org/][Leiningen]]
  - [[https://www.npmjs.com/][npm | build amazing things]] 
** Other cool software and libraries used
- [[https://github.com/fniessen/org-html-themes][fniessen/org-html-themes: How to export Org mode files into awesome HTML in 2 minutes]] 
- [[https://www.gnu.org/software/emacs/][GNU Emacs - GNU Project]] 
- [[https://orgmode.org/][Org mode for Emacs]]


** Libraries not used... but interesting
- [[https://github.com/jbkunst/chessboardjs-themes][jbkunst/chessboardjs-themes: A try to make chessboardjs pieces and color themes]] 

** Clojure helpful links                                            :clojure:
*** Closure compiler
- Using goog closure library
  - [[http://clojurescriptmadeeasy.com/blog/do-not-forget-about-google-closure.html][Don't forget about Google Closure]] 
  - [[https://clojurescript.org/reference/google-closure-library][ClojureScript - Google Closure Library]]
- Google's closure library
  - Code: [[https://github.com/google/closure-library/blob/master/closure/goog/dom/tagname.js][closure-library/tagname.js at master · google/closure-library]] 
  - Auto-generated docs: [[https://google.github.io/closure-library/api/][Index]]
  - Docs: [[https://developers.google.com/closure/compiler/docs/overview][Closure Compiler Documentation  |  Google Developers]] 
  - Releases logs: [[https://github.com/google/closure-library/releases?after=v20200517][Releases · google/closure-library]] 
- Check which version of Closure is used in any Clojurescript version:
  - [[https://cljs.github.io/versions][CLJS Versions]] 
  - [[https://github.com/clojure/clojurescript/blob/master/changes.md][clojurescript/changes.md at master · clojure/clojurescript]] 
*** How to use external code
- Basic infos (good to start here for beginners)
  - Note: leiningen does not mangae js packages. There are some lein-plugins, or
    one can manage js deps with another tool (say lein-deps with lein and
    js-deps with npm).
  - [[https://www.freecodecamp.org/news/why-clojurescript-works-so-well-with-npm-128221d302ba/][Why ClojureScript works so well with NPM]] 
- Best explanations (of using external js libs):
  - [[http://lukevanderhart.com/2011/09/30/using-javascript-and-clojurescript.html][Using JavaScript libraries in ClojureScript.]]
  - [[https://lambdaisland.com/episodes/javascript-libraries-clojurescript#/show_notes][Lambda Island | 18. Using JavaScript libraries in ClojureScript]] 
  - [[https://clojureverse.org/t/guide-on-how-to-use-import-npm-modules-packages-in-clojurescript/2298][Guide on how to use/import npm modules/packages in ClojureScript? - shadow-cljs - ClojureVerse]] 
  - [[https://books.google.de/books?id=uvaWWZ1naikC&pg=PA64&lpg=PA64&dq=foreign+libs+google+closure&source=bl&ots=fIFJOjChQY&sig=nTU5rcitoWbP93A-qRXG8gXruTA&hl=en&sa=X&ei=vhhfUpSPDozbiwLZl4CYCg&redir_esc=y#v=onepage&q=foreign%20libs%20google%20closure&f=false][ClojureScript: Up and Running: Functional Programming for the Web - Stuart Sierra, Luke VanderHart - Google Books]] 
- Official docs
  - [[https://clojurescript.org/news/2017-07-30-global-exports][ClojureScript - Global Exports for Foreign Libraries]]
  - [[https://clojurescript.org/news/2017-07-12-clojurescript-is-not-an-island-integrating-node-modules][ClojureScript - ClojureScript is not an Island: Integrating Node Modules]]
  - [[https://clojurescript.org/guides/webpack][ClojureScript - ClojureScript with Webpack]] 
  - [[https://clojurescript.org/guides/externs#externs-inference][ClojureScript - Externs - externs-inference]] Not used... yet!
  - [[https://clojurescript.org/guides/javascript-modules][ClojureScript - JavaScript Modules (Alpha)]] 
  - Reference
    - [[https://clojurescript.org/reference/dependencies#bundling-javascript-code][ClojureScript - Dependencies - bundling-javascript-code]]
    - [[https://clojurescript.org/reference/compiler-options#target][ClojureScript - Compiler Options - target]]
    - [[https://clojurescript.org/reference/compiler-options#bundle-cmd][ClojureScript - Compiler Options - bundle-cmd]]
    - [[https://clojurescript.org/reference/compiler-options#foreign-libs][ClojureScript - Compiler Options - foreign-libs]]
- Figwheel with npm and webpack
  - [[https://figwheel.org/docs/npm.html][Using NPM | figwheel-main]]
  - [[https://figwheel.org/config-options#final-output-to][figwheel-main | config options - :final-output-to]]
- Barebones: Using only cljs.build.api
  - [[https://anmonteiro.com/2017/03/requiring-node-js-modules-from-clojurescript-namespaces/][Requiring Node.js modules from ClojureScript namespaces · anmonteiro]] 
  
*** Code splitting?
- [[https://clojurescript.org/guides/code-splitting][ClojureScript - Code Splitting]] 
* Code

#+begin_src python
print("Hello")
#+end_src

* Chess notation in source blocks
For more algebraic chess notations, see: [[https://en.wikipedia.org/wiki/Algebraic_notation_(chess)][Algebraic notation (chess) - Wikipedia]].

** Examples of chess.js with chessboard.js

*** Small board
#+begin_export html
<div id="small-board-start" class="small-board"></div>
#+end_export

*** Play with random computer
Ported from: [[https://chessboardjs.com/examples#5001][chessboardjs.com » Examples - 5001]]
#+begin_export html
<div id="play-with-random" style="width: 400px"></div>
#+end_export


** PGN format

*** Wikipedia example
Whole game in PGN format taken from: [[https://en.wikipedia.org/wiki/Portable_Game_Notation][Portable Game Notation - Wikipedia]] 

#+begin_src n-chess-pgn
[Event "F/S Return Match"]
[Site "Belgrade, Serbia JUG"]
[Date "1992.11.04"]
[Round "29"]
[White "Fischer, Robert J."]
[Black "Spassky, Boris V."]
[Result "1/2-1/2"]

1. e4 e5 2. Nf3 Nc6 3. Bb5 a6 {This opening is called the Ruy Lopez.}
4. Ba4 Nf6 5. O-O Be7 6. Re1 b5 7. Bb3 d6 8. c3 O-O 9. h3 Nb8 10. d4 Nbd7
11. c4 c6 12. cxb5 axb5 13. Nc3 Bb7 14. Bg5 b4 15. Nb1 h6 16. Bh4 c5 17. dxe5
Nxe4 18. Bxe7 Qxe7 19. exd6 Qf6 20. Nbd2 Nxd6 21. Nc4 Nxc4 22. Bxc4 Nb6
23. Ne5 Rae8 24. Bxf7+ Rxf7 25. Nxf7 Rxe1+ 26. Qxe1 Kxf7 27. Qe3 Qg5 28. Qxg5
hxg5 29. b3 Ke6 30. a3 Kd6 31. axb4 cxb4 32. Ra5 Nd5 33. f3 Bc8 34. Kf2 Bf5
35. Ra7 g6 36. Ra6+ Kc5 37. Ke1 Nf4 38. g3 Nxh3 39. Kd2 Kb5 40. Rd6 Kc5 41. Ra6
Nf2 42. g4 Bd3 43. Re6 1/2-1/2
#+end_src

*** Annotated correspondence chess

Taken from: [[https://pgnchessbook.org/wp-content/uploads/2019/08/Correspondence_Games.pgn][PGN Chessbook - Correspondence_Games.pgn]]

**** 1

#+begin_src n-chess-pgn
[Event "Scotland-CIF (7)"]
[Site "?"]
[Date "2000.05.08"]
[Round "?"]
[White "Armstrong, A."]
[Black "Stummerer, A."]
[Result "1-0"]
[ECO "B15"]
[WhiteElo "2176"]
[BlackElo "2143"]
[Annotator "Alan Armstrong"]
[PlyCount "35"]

{This game was played by e-mail at a not too hectic pace, the game on the other
hand was...} 1. d4 c6 2. Nc3 d5 { Well my opponent might be playing a
Caro-Kann, but I have news for him...B.D.G} 3. e4 dxe4 4. f3 exf3 5. Nxf3 Bg4
6. h3 {After this move Black can opt to play the Bishop back, instead he
decides to swop off the pieces.} 6... Bxf3 7. Qxf3 Qxd4 {Black is now up by 2
pawns, can White recover, yes by developing and attacking at the same time.} 8.
Be3 Qf6 9. Qg3 { Black would love to swop off the Queen's, but that is not my
poison.} 9... e5 10. O-O-O {White has now reached a safe castled position, and
has much more artillery at his disposal.} 10... Nd7 11. Bc4 {Eyeing up the weak
f7 square, the Rook on h1 will come to f1 and further embarace the black
Queen.} 11... Nh6 12. Rhf1 Qg6 {This time I will oblige Black with the Queen
swop, and then hack away the remnants of the defence.} 13. Qxg6 fxg6 (13...hxg6
14.Bxh6 gxh6 15.Bxf7+ Ke7 16.Bxg6 { In this variation, White regains the 2
pawns and gets the upper hand.} 16...Rg8 17.Rf7+ Ke6 {Forced as Kd8 or e8 will
lead to mate.}) 14. Bxh6 gxh6 15. Ne4 {Now it starts to get very messy, the
Knight on d7 is very vunerable now. Black is still 2 pawns up, but the Bishop
to e6 is too strong.} 15... O-O-O $6 16. Be6 {Followed by Knight to f6 Black
will lose the piece or drop the pawns on h7,h6 and g6.} 16... Kc7 (16...Be7
17.Rf7 Rhe8 18.Rxd7 Rxd7 19.Nf6 Bxf6 20.Bxd7+ Kd8 21.Bxe8 Be7 22.Bxc6 bxc6
23.Rxh7 { Again White's position is much better.}) 17. Rf7 Kb6 18. Rdxd7
{Alfred decided enough was enough, and so another bloody victory for the B. D.
G.} 1-0

#+end_src

**** COMMENT 2 For some reason this one isnt working...
#+begin_src n-chess-pgn
[Event "SCCA Premiers"]
[Site "?"]
[Date "2000.??.??"]
[Round "?"]
[White "Murphy, George"]
[Black "Beacon, Robert"]
[Result "0-1"]
[ECO "D08"]
[Annotator "Robert Beacon"]
[PlyCount "74"]
[EventDate "2000.11.18"]

1. d4 d5 2. c4 e5 {The Albin Countergambit!} 3. dxe5 d4 4. Nf3 Nc6 5. Nbd2 {
#Normal here is 5 g3,but the game soon transposes.} 5... f6 {5...f6 is an
attempt to play the game as a "pure"gambit Alternatives for Black involve
playing the White squared Bishop to g4 or e6, the Queen to d7and o-o-o 
swapping off the Bishops once White has played g3 and Bg2,also in recent years
castling short and playing the game positionally has become popular!} 6. exf6
Nxf6 7. g3 Bg4 8. a3 Qe7 {Normal would be 8...Qd7 as mentioned The text is an
idea of Nikolay Minev in Inside Chess I'm following the game Lignell-Niemela
1941.} 9. Bg2 d3 $1 10. e3 Nd4 11. O-O { 11 h3 was obligatory according to
Minev.} 11... Ne2+ 12. Kh1 O-O-O 13. Qa4 { 13 b4 was played in the above
mentioned game (if 13 h3  h5! )I'm now on my own! } 13... Kb8 14. b4 h5 15. Bb2
h4 16. Nxh4 Bd7 17. Qa5 Ng4 18. Ndf3 Qe8 19. Ne5 {#19 Qg5 wth the idea of 20
Qg6!?} 19... Rh5 20. Nxd7+ Rxd7 21. Qa4 g5 { For me it is "all or nothing" in
this position It is difficult to say what the alternatives are.} 22. Bf3 Qe6
23. Kg2 $2 {The game now swings in Black's favour Possibly 23 Qb5 to bring the
Queen into the game would be better.} 23... gxh4 24. h3 Nxe3+ {( forced )} 25.
fxe3 Rg5 26. Bg4 Qe4+ 27. Rf3 Rf7 { The pressure now builds on White.} 28. Raf1
Nxg3 { With hindsight X followed by Y looks stronger.} 29. R1f2
Nf5 { Black throws away some of his advantage 29... Ne2 is the move!} 30. Kg1
Bd6 31. Rxf5 {#This is probably the decisive mistake At this point the game was
finely balanced  31 c5!? looks better.} 31... Rfxf5 32. Rxf5 { 32 Bd4 prolongs
the game.} 32... Qxe3+ (32...Rxg4+ 33.hxg4 Qxg4+ 34.Kh1 Qh3+ 35.Kg1 Qh2+ 36.Kf1
Qh1+ 37.Kf2 Bg3# {would be more percise.}) 33. Kf1 Rxf5+ 34. Bxf5 Bg3 $1 {In a
lot of lines in the Albin White's Queen goes to a4 to pressure Black's
queenside In this instance it was his undoing as it remained out of the game
The back rank threat was an allusion!} 35. Bd4 { The Bishop threat comes too
late.} 35... Qe1+ 36. Kg2 Qe2+ 37. Kg1 Qh2+ 0-1
#+end_src

**** COMMENT 2 Original --> chess.js only loaded pgn till move 27
#+begin_src n-chess-pgn
[Event "SCCA Premiers"]
[Site "?"]
[Date "2000.??.??"]
[Round "?"]
[White "Murphy, George"]
[Black "Beacon, Robert"]
[Result "0-1"]
[ECO "D08"]
[Annotator "Robert Beacon"]
[PlyCount "74"]
[EventDate "2000.11.18"]

1. d4 d5 2. c4 e5 {The Albin Countergambit!} 3. dxe5 d4 4. Nf3 Nc6 5. Nbd2 {
#Normal here is 5 g3,but the game soon transposes.} 5... f6 {5...f6 is an
attempt to play the game as a "pure"gambit Alternatives for Black involve
playing the White squared Bishop to g4 or e6, the Queen to d7and o-o-o 
swapping off the Bishops once White has played g3 and Bg2,also in recent years
castling short and playing the game positionally has become popular!} 6. exf6
Nxf6 7. g3 Bg4 8. a3 Qe7 {Normal would be 8...Qd7 as mentioned The text is an
idea of Nikolay Minev in Inside Chess I'm following the game Lignell-Niemela
1941.} 9. Bg2 d3 $1 10. e3 Nd4 11. O-O { 11 h3 was obligatory according to
Minev.} 11... Ne2+ 12. Kh1 O-O-O 13. Qa4 { 13 b4 was played in the above
mentioned game (if 13 h3  h5! )I'm now on my own! } 13... Kb8 14. b4 h5 15. Bb2
h4 16. Nxh4 Bd7 17. Qa5 Ng4 18. Ndf3 Qe8 19. Ne5 {#19 Qg5 wth the idea of 20
Qg6!?} 19... Rh5 20. Nxd7+ Rxd7 21. Qa4 g5 { For me it is "all or nothing" in
this position It is difficult to say what the alternatives are.} 22. Bf3 Qe6
23. Kg2 $2 {The game now swings in Black's favour Possibly 23 Qb5 to bring the
Queen into the game would be better.} 23... gxh4 24. h3 Nxe3+ {( forced )} 25.
fxe3 Rg5 26. Bg4 Qe4+ 27. Rf3 Rf7 { The pressure now builds on White.} 28. Raf1
Nxg3 { With hindsight 28...Rxg4 followed by 29... h3+ looks stronger.} 29. R1f2
Nf5 { Black throws away some of his advantage 29... Ne2 is the move!} 30. Kg1
Bd6 31. Rxf5 {#This is probably the decisive mistake At this point the game was
finely balanced  31 c5!? looks better.} 31... Rfxf5 32. Rxf5 { 32 Bd4 prolongs
the game.} 32... Qxe3+ (32...Rxg4+ 33.hxg4 Qxg4+ 34.Kh1 Qh3+ 35.Kg1 Qh2+ 36.Kf1
Qh1+ 37.Kf2 Bg3# {would be more percise.}) 33. Kf1 Rxf5+ 34. Bxf5 Bg3 $1 {In a
lot of lines in the Albin White's Queen goes to a4 to pressure Black's
queenside In this instance it was his undoing as it remained out of the game
The back rank threat was an allusion!} 35. Bd4 { The Bishop threat comes too
late.} 35... Qe1+ 36. Kg2 Qe2+ 37. Kg1 Qh2+ 0-1
#+end_src

** FEN notation
Example taken from: [[https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation][Forsyth–Edwards Notation - Wikipedia]] 

Here's the FEN for the starting position:


#+begin_src n-chess-fen
rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1
#+end_src

And after the move 1.e4:

#+begin_src n-chess-fen
rnbqkbnr/pppppppp/8/8/4P3/8/PPPP1PPP/RNBQKBNR b KQkq e3 0 1
#+end_src

And then after 1...c5:

#+begin_src n-chess-fen
rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq c6 0 2
#+end_src

And then after 2.Nf3:

#+begin_src n-chess-fen
rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 1 2
#+end_src


* Only text
Some paragraph here and there.

Hello!
* Another list. This one one level 1.
- Hi 
- Hello
- Hallo!
- Ola!
* Same list... just longer.
- Hi 
- Hello
- Hallo!
- Ola!
- Hi 
- Hello
- Hallo!
- Ola!
- Hi 
- Hello
- Hallo!
- Ola!
- Hi 
- Hello
- Hallo!
- Ola!
- Hi 
- Hello
- Hallo!
- Ola!
- Hi 
- Hello
- Hallo!
- Ola!
- Hi 
- Hello
- Hallo!
- Ola!
* Another list, but longer items
- AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAAAAA AAAA AAAAAA AAAA AAAAAAA AAA A AAAAAAAAA
- BB BBBBBBBBBB BBBBB B BBBBBBBBBB BBBBBB BBBBBBB B B B B B B BBBBBBB BBBBBBBB B B BBB BBB BB
- CCCCCCCCCCC CCCCCCC CCCCCCCCC CCCCCCCCCC CCCCCCCC CCCCCCCC CCCCCCC CCCCC C CC CCCC
- DDDDDD DDDDDDDD DDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDD DDDDDD DDD DDDDD
- AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAAAAA AAAA AAAAAA AAAA AAAAAAA AAA A AAAAAAAAA
- BB BBBBBBBBBB BBBBB B BBBBBBBBBB BBBBBB BBBBBBB B B B B B B BBBBBBB BBBBBBBB B B BBB BBB BB
- CCCCCCCCCCC CCCCCCC CCCCCCCCC CCCCCCCCCC CCCCCCCC CCCCCCCC CCCCCCC CCCCC C CC CCCC
- DDDDDD DDDDDDDD DDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDD DDDDDD DDD DDDDD
- AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAAAAA AAAA AAAAAA AAAA AAAAAAA AAA A AAAAAAAAA
- BB BBBBBBBBBB BBBBB B BBBBBBBBBB BBBBBB BBBBBBB B B B B B B BBBBBBB BBBBBBBB B B BBB BBB BB
- CCCCCCCCCCC CCCCCCC CCCCCCCCC CCCCCCCCCC CCCCCCCC CCCCCCCC CCCCCCC CCCCC C CC CCCC
- DDDDDD DDDDDDDD DDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDD DDDDDD DDD DDDDD
- AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAAAAA AAAA AAAAAA AAAA AAAAAAA AAA A AAAAAAAAA
- BB BBBBBBBBBB BBBBB B BBBBBBBBBB BBBBBB BBBBBBB B B B B B B BBBBBBB BBBBBBBB B B BBB BBB BB
- CCCCCCCCCCC CCCCCCC CCCCCCCCC CCCCCCCCCC CCCCCCCC CCCCCCCC CCCCCCC CCCCC C CC CCCC
- DDDDDD DDDDDDDD DDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDD DDDDDD DDD DDDDD
- AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAAAAA AAAA AAAAAA AAAA AAAAAAA AAA A AAAAAAAAA
- BB BBBBBBBBBB BBBBB B BBBBBBBBBB BBBBBB BBBBBBB B B B B B B BBBBBBB BBBBBBBB B B BBB BBB BB
- CCCCCCCCCCC CCCCCCC CCCCCCCCC CCCCCCCCCC CCCCCCCC CCCCCCCC CCCCCCC CCCCC C CC CCCC
- DDDDDD DDDDDDDD DDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDD DDDDDD DDD DDDDD
- AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAAAAA AAAA AAAAAA AAAA AAAAAAA AAA A AAAAAAAAA
- BB BBBBBBBBBB BBBBB B BBBBBBBBBB BBBBBB BBBBBBB B B B B B B BBBBBBB BBBBBBBB B B BBB BBB BB
- CCCCCCCCCCC CCCCCCC CCCCCCCCC CCCCCCCCCC CCCCCCCC CCCCCCCC CCCCCCC CCCCC C CC CCCC
- DDDDDD DDDDDDDD DDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDD DDDDDD DDD DDDDD


* Same list, but formated differently
- AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAAAAA AAAA AAAAAA AAAA AAAAAAA AAA A
  AAAAAAAAA
- BB BBBBBBBBBB BBBBB B BBBBBBBBBB BBBBBB BBBBBBB B B B B B B BBBBBBB BBBBBBBB
  B B BBB BBB BB
- CCCCCCCCCCC CCCCCCC CCCCCCCCC CCCCCCCCCC CCCCCCCC CCCCCCCC CCCCCCC CCCCC C
  CC CCCC
- DDDDDD DDDDDDDD DDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDD DDDDDD DDD
  DDDDD
- AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAAAAA AAAA AAAAAA AAAA AAAAAAA AAA A
  AAAAAAAAA
- BB BBBBBBBBBB BBBBB B BBBBBBBBBB BBBBBB BBBBBBB B B B B B B BBBBBBB BBBBBBBB
  B B BBB BBB BB
- CCCCCCCCCCC CCCCCCC CCCCCCCCC CCCCCCCCCC CCCCCCCC CCCCCCCC CCCCCCC CCCCC C
  CC CCCC
- DDDDDD DDDDDDDD DDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDD DDDDDD DDD
  DDDDD
- AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAAAAA AAAA AAAAAA AAAA AAAAAAA AAA A
  AAAAAAAAA
- BB BBBBBBBBBB BBBBB B BBBBBBBBBB BBBBBB BBBBBBB B B B B B B BBBBBBB BBBBBBBB
  B B BBB BBB BB
- CCCCCCCCCCC CCCCCCC CCCCCCCCC CCCCCCCCCC CCCCCCCC CCCCCCCC CCCCCCC CCCCC C
  CC CCCC
- DDDDDD DDDDDDDD DDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDD DDDDDD DDD
  DDDDD
- AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAAAAA AAAA AAAAAA AAAA AAAAAAA AAA A
  AAAAAAAAA
- BB BBBBBBBBBB BBBBB B BBBBBBBBBB BBBBBB BBBBBBB B B B B B B BBBBBBB BBBBBBBB
  B B BBB BBB BB
- CCCCCCCCCCC CCCCCCC CCCCCCCCC CCCCCCCCCC CCCCCCCC CCCCCCCC CCCCCCC CCCCC C
  CC CCCC
- DDDDDD DDDDDDDD DDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDD DDDDDD DDD
  DDDDD
- AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAAAAA AAAA AAAAAA AAAA AAAAAAA AAA A
  AAAAAAAAA
- BB BBBBBBBBBB BBBBB B BBBBBBBBBB BBBBBB BBBBBBB B B B B B B BBBBBBB BBBBBBBB
  B B BBB BBB BB
- CCCCCCCCCCC CCCCCCC CCCCCCCCC CCCCCCCCCC CCCCCCCC CCCCCCCC CCCCCCC CCCCC C
  CC CCCC
- DDDDDD DDDDDDDD DDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDD DDDDDD DDD
  DDDDD
- AAAAA AAAAA AAAAA AAAAA AAAAA AAAAA AAAAAAAA AAAA AAAAAA AAAA AAAAAAA AAA A
  AAAAAAAAA
- BB BBBBBBBBBB BBBBB B BBBBBBBBBB BBBBBB BBBBBBB B B B B B B BBBBBBB BBBBBBBB
  B B BBB BBB BB
- CCCCCCCCCCC CCCCCCC CCCCCCCCC CCCCCCCCCC CCCCCCCC CCCCCCCC CCCCCCC CCCCC C
  CC CCCC
- DDDDDD DDDDDDDD DDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDDD DDDDDDDDDDD DDDDDD DDD
  DDDDD


* And an empty list
* COMMENT My tags
Some comment that isn't going to be exported.
** COMMENT My tags
- favs
- pet
- wild
- h1
- h2 
- best

