(ns org-html-cljs-fns.test-runner
  (:require
   [cljs-test-display.core]
   [figwheel.main.testing :refer [run-tests-async]]
   ;; require all the namespaces that you want to test
   [org-html-cljs-fns.chess-src-blocks-test]))

;; This isn't strictly necessary, but is a good idea depending
;; upon your application's ultimate runtime engine.
;;(enable-console-print!)


(defn -main [& args]
  ;; this needs to be the last statement in the main function so that it can
  ;; return the value `[:figwheel.main.async-result/wait 10000]`
  (run-tests-async 5000)
  )
