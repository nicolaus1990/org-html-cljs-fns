(ns org-html-cljs-fns.chess-src-blocks
    (:require [clojure.string :as cljStr]
              [clojure.set :as cset]
              [goog.dom :as dom]
              [goog.dom.TagName :as TagName]
              [goog.events :as evts]
              ;;chess.js
              [npm.chessjs]
              ;;[cljsjs.chess.js]
              [npm.chessboardjs]
              ;;["chess.js" :refer [Chess]]
              ;;["@chrisoakman/chessboardjs/dist/chessboard-1.0.0.js" :as cbj]
              ;;["@chrisoakman/chessboardjs/dist/chessboard-1.0.0.min.js"]
              ;;["@chrisoakman/chessboardjs"]
              ;;[chessboardjs]
              ;;["@chrisoakman/chessboardjs" :as chess-board-js]
              ;;[chessboardjs :as cbj]
              [org-html-cljs-fns.config :as config]
              [org-html-cljs-fns.chess-src-blocks-examples :as chess-examples]
              ;;[org-html-cljs-fns.p5-cljs-core :as p]
              [org-html-cljs-fns.tools :as t :refer [log log-ind error alert]])
    ;; In :import we import classes and enums (and only libs that are compatible
    ;; with Google's Closure library).
    ;; - EventType is Enum
    (:import [goog.events EventType]))

(def org-mode-src-block-name-chess-fen "n-chess-fen")
(def org-mode-src-block-name-chess-pgn "n-chess-pgn")

;; CSS classes of src-blocks (this is determined by org-mode: lang of
;; src-block prefixed with "src-").
(def fen (str "src-" org-mode-src-block-name-chess-fen))
(def pgn (str "src-" org-mode-src-block-name-chess-pgn))

;; Chess global state

(def chess-boards (atom []))

;;-----------------------------------------
;; Test foreign non-closure npm libs are loaded.

(defn start-board [{:keys [id position showNotation]
                    :or {showNotation true}
                    :as args}]
  "Renders chessboard in dom. Returns chessboard.js object.

Assuming that there is a div:
    <div id=\"board1\" style=\"width: 400px\"></div>
Should return the js equivalent of something like:
    var board1 = Chessboard('board1', {
      //position: 'start',
      position: 'r1bqkbnr/pppp1ppp/2n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R'
      showNotation: false
    })
"
  (js/Chessboard id (clj->js args)
                 ;; This works: #js {:position position :showNotation showNotation}
                 ;; This does not: #js args because: #js needs literal map or vector
                 ))

#_(comment
  (js/Chessboard "myTestBoard" #js {:position "r1bqkbnr/pppp1ppp/2n5/1B2p3/4P3/5N2/PPPP1PPP/RNBQK2R"})
  )

(defn get-node-id [& {:keys [prefix-id type]
                      :or {prefix-id ""}}]
  "One fn to get ids of html elements."
  (case type
    :pgn-start       (str "pgn_button_start_"      prefix-id)
    :pgn-next-move   (str "pgn_button_next_move_"  prefix-id)
    :pgn-prev-move   (str "pgn_button_prev_move_"  prefix-id)
    :pgn-comment     (str "pgn_button_comment_"    prefix-id)
    ;;default:
    (js/alert (str "Fn get-node-id not implemented for type: " type ", id: " prefix-id))))

(defn chessjs-fen->chessboardjs-fen [fen-str]
  (first (re-seq #"[rnbqkRNBQKpP/12345678]+" fen-str)))


(defmulti generate-board
  "Renders board. Should return a map with :chessboard-obj and evtl
  also :chess-logic"
  (fn [notation-type _] notation-type))

(defmethod generate-board fen [_ {:keys [id content] :as args}]
  #_(println (str "generate-board fen. id: " id))
  #_(println (str "ARGS: " args))
  (if-let [fen (chessjs-fen->chessboardjs-fen content)]
    {:chessboard-obj (start-board (-> args
                                      (assoc :content fen)
                                      (assoc :showNotation false)
                                      (cset/rename-keys {:content :position})))}
    (println (str "FEN didn't even pass simple regex validation: " content))))

(defmethod generate-board pgn [_ {:keys [id content comment-node] :as args}]
  (let [button-start    (dom/getElement (get-node-id :prefix-id id :type :pgn-start))
        button-next     (dom/getElement (get-node-id :prefix-id id :type :pgn-next-move))
        button-prev     (dom/getElement (get-node-id :prefix-id id :type :pgn-prev-move))
        button-comment  (dom/getElement (get-node-id :prefix-id id :type :pgn-comment))
        ;; This inits chess.js obj and loads pgn.
        chess-logic (doto (js/Chess) (.load_pgn content))
        chess-comments (.get_comments chess-logic)
        ;;start-board renders board.
        chessboard-obj (start-board (-> args
                                        (assoc :content (.fen chess-logic))
                                        (cset/rename-keys {:content :position})))]
    (let [all-moves (.history chess-logic #js {:verbose true})
          total-num-moves (count all-moves)
          ;; The fen of chess-logic after pgn load is last board.
          current-move-index (atom (dec total-num-moves))
          hide-comments (atom true)]
      #_(do (println "") (println "All moves in pgn:") (println all-moves) (println ""))
      (letfn [(update-comment []
                ;; Showing comments isnt that simple, since chess-logic
                ;; (api) only returns comments with its fen (without turn
                ;; number of pgn or other helpful info)
                (when (not @hide-comments)
                  (let [comment-objs chess-comments
                        move-num (int (/ @current-move-index 2))]
                    (dom/setTextContent comment-node (str "Move (" move-num  "). Comment..."))
                    ;; If there is no comment-objs, do nothing.
                    (when comment-objs
                      (if-let [comment-found
                               (->> comment-objs
                                    (map js->clj)
                                    (some (fn [c]
                                            (let [current-fen (chessjs-fen->chessboardjs-fen (.fen chess-logic))
                                                  fen (chessjs-fen->chessboardjs-fen (get c "fen"))]
                                              #_(do (println "") (println "Comment: " (get c "comment")) (println current-fen) #_(println comment-objs) #_(println ""))
                                              #_(do #_(println c) (println fen " <-- given") (println (chessjs-fen->chessboardjs-fen fen))#_(println ""))
                                              (and (= current-fen fen) (get c "comment"))))))]
                        (do
                          #_(do (println comment-found) (println "SAME!"))
                          (dom/setTextContent comment-node (str "Move (" move-num
                                                                "). Comment: " comment-found))))))))
              (re-render [new-index]
                ;; Reset (i) and then just re-render (ii)
                ;; (i)
                (.reset chess-logic)
                (let [only-these-moves (drop-last (- total-num-moves new-index) all-moves)]
                  ;;(println (str "Num of in game so far: " (count only-these-moves)))
                  (doseq [move only-these-moves]
                    (.move chess-logic move)))
                ;; (ii)
                (as-> chess-logic $
                  (.fen $)
                  (chessjs-fen->chessboardjs-fen $)
                  (.position chessboard-obj $ #js {:useAnimation false}))
                (update-comment))
              (start-over []
                (-> (reset! current-move-index 0)
                    re-render))
              (next-move []
                (when (<= (inc @current-move-index) total-num-moves)
                  (let [new-index (swap! current-move-index inc)]
                    ;;(println (str "Next click. index: " new-index))
                    (re-render new-index))))
              (prev-move []
                (when (< 0 @current-move-index)
                  (let [new-index (swap! current-move-index dec)]
                    ;;(println (str "Prev click. index: " new-index))
                    (re-render new-index))))
              (toggle-comment []
                (if (reset! hide-comments (not @hide-comments))
                  (dom/setTextContent comment-node "")
                  (update-comment)))]
        (do (toggle-comment)
            (evts/listen button-next    EventType.CLICK next-move)
            (evts/listen button-prev    EventType.CLICK prev-move)
            (evts/listen button-start   EventType.CLICK start-over)
            (evts/listen button-comment EventType.CLICK toggle-comment)
            #_(println (.ascii chess-logic))
            {:chess-logic chess-logic
             :chessboard-obj chessboard-obj})))))

;;(def m-fn (let [s (atom 1)] (letfn [(my-inc [] (swap! s inc) @s)] my-inc)))

(defmethod generate-board :default [nt _]
  (println (str "No method for notation-type " nt " implemented.")))


(defmulti insert-div-for-chessboard
  "Returns newly inserted div that will eventually contain chessboard."
  (fn [notation-type _] notation-type))

(defmethod insert-div-for-chessboard fen [_ {:keys [id content src-node-parent]}]
  (let [div-node (dom/createElement TagName/DIV)]
    #_(println id)
    {:chessboard-node (doto div-node ;; doto returns div-node
                       (dom/setProperties #js {:id id :style "width: 300px" ;;:class "small-board"
                                                    })
                       #_(dom/setTextContent "Chessboard: ")
                       (dom/insertSiblingBefore src-node-parent))}))


(defmethod insert-div-for-chessboard pgn [_ {:keys [id content src-node src-node-parent]}]
  (let [div-node (dom/createElement TagName/DIV)
        button-start (doto (dom/createElement TagName/BUTTON)
                       (dom/setProperties #js {:id (get-node-id :prefix-id id :type :pgn-start)})
                       (dom/setTextContent "Beginning"))
        button-next  (doto (dom/createElement TagName/BUTTON)
                       (dom/setProperties #js {:id (get-node-id :prefix-id id :type :pgn-next-move)})
                       (dom/setTextContent "Next move"))
        button-prev  (doto (dom/createElement TagName/BUTTON)
                       (dom/setProperties #js {:id (get-node-id :prefix-id id :type :pgn-prev-move)})
                       (dom/setTextContent "Prev move"))
        button-pgn-text  (doto (dom/createElement TagName/BUTTON)
                           (dom/setProperties #js {:id (get-node-id :prefix-id id :type :pgn-comment)})
                           (dom/setTextContent "comment"))]
    #_(println id)
    (doto div-node 
        (dom/setProperties #js {:id id :style "width: 350px"})
        #_(dom/setTextContent "Chessboard: ")
        (dom/insertSiblingBefore src-node-parent))
    (do (dom/insertSiblingAfter button-next     div-node)
        (dom/insertSiblingAfter button-prev     div-node)
        (dom/insertSiblingAfter button-pgn-text div-node)
        (dom/insertSiblingAfter button-start    div-node))
    {:chessboard-node div-node
     :comment-node (doto (dom/createElement TagName/DIV)
                     (dom/setProperties #js {:style "margin-bottom: 30px;margin-top: 30px;"})
                     (dom/insertSiblingBefore src-node))}))


(defn add-chess-boards []
  ;; Org mode source blocks :=  <div class="org-src-container"><pre class="src">some code</pre></div>
  (letfn [(add-chess-board [nt board-info]
            (swap! chess-boards conj (merge board-info
                                            (generate-board nt board-info))))
          #_(print-and-ret [msg objs] (do (println msg) objs))]
    (doseq [notation-type [fen pgn]]
      (doseq [src-block-node (array-seq (dom/getElementsByClass notation-type))]
        (try
          (let [bi (-> {}
                       (assoc :id (str (random-uuid)))
                       (assoc :notation-type notation-type)
                       (assoc :content (.. src-block-node -innerHTML))
                       (assoc :src-node src-block-node)
                       (assoc :src-node-parent (dom/getParentElement src-block-node)))]
            (->> (insert-div-for-chessboard notation-type bi)
                 (merge bi )
                 (add-chess-board notation-type)))
          #_(catch js/Error e
              (println "*** ERROR ***")
              (println "Some error was thrown.")
              (println "***"))
          #_(catch :default e
            (println (str "JavaScript unfortunately allows you to throw anything. Thing thrown: " e))
            (throw e)))))))

#_(comment
    (ns org-html-cljs-fns.chess-src-blocks)
    ;; Gets fen:
    ((.. (:chessboard-obj (first @chess-boards)) -fen))
    )


(defn run []
  ;;(let [c (dom/getElement "content")])
  (add-chess-boards)
  #_(when config/debug?
    (chess-examples/run)))

;;; Something
(defn figwheel-before-load []
  (log "Before reload (chess-src-blocks)")
  (doseq [board @chess-boards]
    (dom/setProperties (:chessboard-node board)  #js {:class "hide"})
    (dom/setTextContent (:src-node board) (:content board))
    (reset! chess-boards []))
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
  )
