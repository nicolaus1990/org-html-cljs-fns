(ns org-html-cljs-fns.app-lists.views.re-com-utils
  (:require [org-html-cljs-fns.tools :as t :refer [rand-str log log-ind error alert]]
            ;; Re-com (css)
            [re-com.core    :as rc  :refer [h-box v-box box gap line border title label
                                            single-dropdown modal-panel progress-bar input-text
                                            label checkbox button hyperlink-href p p-span]]
            [re-com.modal-panel     :refer [modal-panel-args-desc]]
            [re-com.dropdown :refer [filter-choices-by-keyword single-dropdown-args-desc]]))

;;----------------------------------------------------
;; Requirements:
;;      - re-com.css
;;      - material-design-iconic-font.min.css
;;      - chosen-sprite.png
;;      - chosen-sprite@2x.png
;;----------------------------------------------------



;;----------------------------------------------------
;; Basic re-com stuff
;;----------------------------------------------------


(comment "Override re-com basic default css
* {
    min-height: auto;
    min-width: auto;
}
html, body {
    height: auto;
}
")

;;----------------------------------------------------
;; Single-dropdown select menu but looks more like a button
;;----------------------------------------------------


(defn simple-dd-select-button [hiccup-fn choices on-change-fn & {:keys [width model placeholder]
                                                                 ;;Default values:
                                                                 :or {width "auto" model nil placeholder "..."}}]
  "Required args: hiccup-fn choices on-change-fn
   Optional: keys as established by re-com.core/single-dropdown.
  Simple drop down menu button. Overriding re-com.dropdown.simple-dropdown.
  Requires overriding of css class (see comments below). "
  [h-box
   :gap      "5px"
   :align    :center
   ;;[dd-select-button choices on-change-fn]
   :children [[:span.n-simple-dd-select-button
               [single-dropdown
                :width     width
                :choices   choices
                ;[{:id 1 :label "Select something"}
                ; {:id 2 :label "Display simple demo 2"}]
                ;:model     selected-item     ; the id of a choice
                :model model ; if model is nil, then placeholder is taken
                :placeholder placeholder
                ;;:render-fn (fn [choice] [:span.zmdi.zmdi-flower-alt])
                :on-change                                            ;;#(log (str "root-menu-button: Selected id: " %))
                on-change-fn]]
              [box
               :size "auto"
               :child (hiccup-fn)]]])

;; Usage example:
#_(rcu/simple-dd-select-button
    (fn [] [:h3 "My lists"])
    [{:id 1 :label "Select something"}
     {:id 2 :label "Display simple demo 2"}
     {:id 3 :label "Display simple demo 3"}]
    (fn [id] (do (log "root-menu-button")
                 (log "     Selected id: " id))))

(comment
  "simple-dd-menu-button requires to overwrite some re-com css rules.
  Note: PATH_TO_ASSET_PNG is a dependency of re-com:
  PATH_TO_ASSET_PNG := \"../assets/css/chosen-sprite.png\" or \"assets/css/chosen-sprite.png\"

  /* Overriding some css to make re-com single-dropdown menu like a button
 Prefix stuff with: .n-simple-dd-select-button>div.rc-dropdown>
 */
.n-simple-dd-select-button>div.rc-dropdown>a.chosen-single.chosen-default {
  color: red;
  width: 20px;
  border: inherit;
  background-color: inherit;
  box-shadow: inherit;
}
.n-simple-dd-select-button>div.rc-dropdown>.chosen-single div b {
  background: url(PATH_TO_ASSET_PNG) no-repeat 0 15px;
  display: block;
  height: 100%;
  width: 100%;
}
.n-simple-dd-select-button>div.rc-dropdown>.chosen-drop {
  width: 200px;
}"
  )

