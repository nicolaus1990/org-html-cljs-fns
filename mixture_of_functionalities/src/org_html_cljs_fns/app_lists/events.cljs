(ns org-html-cljs-fns.app-lists.events
  (:require [clojure.string :as cljStr]
            [clojure.set :as cljSet]
            ;;[goog.events :as evts]
            [org-html-cljs-fns.app-lists.db :as db]
            [re-frame.core :as rf]
            [reagent.core :as r]
            [org-html-cljs-fns.tools :as t :refer [rand-str log log-ind error alert]]))

;;----------------------------------------------------
;; Domino 2 - Event Handlers
;;----------------------------------------------------

(rf/reg-event-db              ;; sets up initial application state
  :initialize                 ;; usage:  (dispatch [:initialize-clock])
  (fn [_ _]                   ;; the two parameters are not important here, so use _
    (db/init-state))) ;; What it returns becomes the new application state



(rf/reg-event-db
  :list-item-drag
  (fn [db [_ event list-id]]
    ;; We do something like: (swap! @lists vector-move (.-oldIndex event) (.-newIndex event))
    ;; but functional.
    (let [lists (db/get-lists db)]
      (let [new-list (db/move-sublist lists list-id (.-oldIndex event) (.-newIndex event))]
        (db/update-lists db new-list)))))

(rf/reg-event-db
  :list-to-display
  (fn [db [_ list-id]]
    (let [new-db (db/update-list-to-display db list-id)]
      (log "*** LIST-TO-DISPLAY *** ")
      (log "*** OLD-DB: " db)
      (log "*** NEW-DB: " new-db)
      new-db
      )))

(rf/reg-event-db
  :list-update
  (fn [db [_ list-id kw val]]
    (let [new-list (db/update-list (db/get-lists db) list-id kw val)]
      (log "*** LIST-UPDATE *** ")
      (log "*** OLD-DB: " db)
      (log "*** NEW-DB: " (db/update-lists db new-list))
      (db/update-lists db new-list)
      )))

(rf/reg-event-db
  :list-remove
  (fn [db [_ list-id]]
    (let [new-list (db/remove-list (db/get-lists db) list-id)]
      (log "*** LIST-REMOVE *** ")
      (log "*** OLD-DB: " db)
      (log "*** NEW-DB: " (db/update-lists db new-list))
      (db/update-lists db new-list)
      )))

(rf/reg-event-db
  :list-add
  ;; Params:
  ;;    - Vector items contains maps, each with keyword :content, :link-mode etc.
  ;;    - Keyword next-to-or-sub: either :sibling or :sublist, depending on where to add list item.
  (fn [db [_ next-to-or-sub list-id items]]
    (let [new-list (db/add-list-many next-to-or-sub
                                     (db/get-lists db)
                                     list-id
                                     items)]
      (log "*** LIST-ADD *** ")
      (log "*** OLD-DB: " db)
      (log "*** NEW-DB: " (db/update-lists db new-list))
      (db/update-lists db new-list)
      )))
