(ns org-html-cljs-fns.app-lists.subs
  (:require [clojure.string :as cljStr]
            [clojure.set :as cljSet]
            ;;[goog.events :as evts]
            [re-frame.core :as rf]
            [org-html-cljs-fns.app-lists.db :as db]
            [org-html-cljs-fns.tools :as t :refer [get-deep-elem rand-str log log-ind error alert]]))

;;----------------------------------------------------
;; Domino 4 - Query
;;----------------------------------------------------
(rf/reg-sub
  :lists ;; Return vector of root-node list
  (fn [db _] ;; db is current app state. 2nd param is query vector
    (db/get-lists db)))

(rf/reg-sub
  :first-level-lists ;; Return vector of root-node list
  (fn [db _] ;; db is current app state. 2nd param is query vector
    (db/get-list-items (db/get-lists db))))

(rf/reg-sub
  :list-id-to-display
  (fn [db _] ;; db is current app state. 2nd param is query vector.
    ;; return a query computation over the application state
    #_(db/get-list-items (db/get-lists db)
                       (db/get-list-id-to-display db))
    (db/get-list-id-to-display db)))

(rf/reg-sub
  :list-by-id
  (fn [db [_ list-obj-id]] ;; db is current app state. 2nd param is query vector
    ;;db
    (db/db-id->list db list-obj-id)
    ;;(db/get-list-items (db/get-lists db) list-obj-id)
    ))