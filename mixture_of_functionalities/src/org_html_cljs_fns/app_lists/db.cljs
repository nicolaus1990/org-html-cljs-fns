(ns org-html-cljs-fns.app-lists.db
  (:require [clojure.string :as cljStr]
            [clojure.set :as cljSet]
            [reagent.core :as r]
            [org-html-cljs-fns.tools :as t :refer [get-deep-elem rand-str log log-ind error alert]]))

;;------- Tools

(def get-list-obj  (fn [xs id] (get-deep-elem xs #(= (:id %) id) #(:xs %))))

;;Fn get-list-obj should end up being something like this:
#_(defn get-list-obj [xs id]
    (if (= (:id xs) id)
      xs
      (if (empty? (:xs xs))
        nil
        ;;some is like ormap, every? is like andmap
        (some #(get-list-obj % id) (:xs xs)))))

(defn get-path-to-list [list-obj list-id] (t/get-path-to #(= (:id %) list-id) list-obj))

;;----------------------------------------------------
;; List app state
;;----------------------------------------------------

(def id-root-list-node "root")

(defn init-lists []
  ;;TODO: some api: https://jsonplaceholder.typicode.com/posts/1
  #_(vec (map (fn [i] (str "Item " i)) (range 10)))
  {:id id-root-list-node :x "All lists"
   :xs [{:id (rand-str) :x "List 1" :xs nil}
        {:id (rand-str) :x "List 2" :xs [{:id (rand-str) :x "List 2.1" :xs [{:id (rand-str) :x "List 2.1.1" :xs nil}
                                                                            {:id (rand-str) :x "List 2.1.2" :xs [{:id (rand-str) :x "List 2.1.2.1" :xs nil}
                                                                                                                 {:id (rand-str) :x "List 2.1.2.2" :xs nil}]}]}]}
        {:id (rand-str) :x "List 3" :xs [{:id (rand-str) :x "List 3.1" :xs nil}
                                         {:id (rand-str) :x "List 3.2" :xs nil}]}
        {:id (rand-str) :x "List 4" :xs [{:id (rand-str) :x "List 4.1" :xs [{:id (rand-str) :x "List 4.1.1" :xs nil}
                                                                            {:id (rand-str) :x "List 4.1.2" :xs nil}]}]}]}
  )

(defn init-state []
  {:lists (init-lists)
   :display id-root-list-node})

(defn make-list [& {:keys [id content link-mode sublists]
                    :or   {:content "" :link-mode nil
                           :sublists nil}}]
  {:id (or id (rand-str)) :x content :xs sublists})

;;----------------------------------------------------
;; Some getters and setters
;;----------------------------------------------------
(def kw-id :id)
(def kw-content :x)
(def kw-sublists :xs)
(def kw-link-list :link-list)

(defn get-list-content [xs] (:x xs))
(defn get-list-is-link-list [xs] (:link-list xs))
(defn get-list-items ;;TODO: Rename to get-list-sublists
  ([xss] (:xs xss))
  ([xss list-id]  (get-list-obj xss list-id)))
(defn get-lists [db] (:lists db));;TODO: Rename to get-state-lists
(defn get-list-id-to-display [db] (:display db)) ;;TODO: rename to get-state-list-id-to-display

(defn get-list-by-id [lists id] (get-list-items lists id))
(defn db-id->list [db id] (get-list-items (get-lists db) id))


(defn update-list-to-display [db id] (assoc-in db [:display] id))

(defn update-lists [db new-xs] (assoc-in db [:lists] new-xs))

(defn move-sublist [lists list-id old-index new-index]
  (do (log (str "List id: " list-id))
      (log (str "Sub lists: " (:xs lists)))
      (if (= list-id "root")
        (do (let [new-lists (update-in lists [:xs] t/vector-move old-index new-index)]
              (log (str "New lists: " (:xs new-lists)))
              new-lists))
        (if-let [path (get-path-to-list lists list-id)]
          (do (log (str "Path: " path))
              (let [new-lists (update-in lists (conj path :xs) t/vector-move old-index new-index)]
                (log (str "New lists: " (:xs new-lists)))
                new-lists))
          (do (log (str "No list found with id: " list-id))
              lists)))))

(defn update-list [lists list-id kw val]
  (do (log (str "List id: " list-id))
      (log (str "Sub lists: " (:xs lists)))
      (log (str "KW: " kw "---> VAL: " val))
      (if (= list-id "root")
        (do (let [new-lists (assoc-in lists [kw] val)]
              (log (str "New lists: " (:xs new-lists)))
              new-lists))
        (if-let [path (get-path-to-list lists list-id)]
          (do (log (str "Path: " path))
              (let [new-lists (assoc-in lists (-> path ;;(conj  :xs)
                                                  (conj  kw)) val)]
                (log (str "New lists: " (:xs new-lists)))
                new-lists))
          (do (log (str "No list found with id: " list-id))
              lists))
        )))

(defn remove-list [lists list-id]
  (do (log (str "List id: " list-id))
      (log (str "Sub lists: " (:xs lists)))
      (if (= list-id "root")
        (log (str "*** Warning: Root-elem list was tried to be removed. ***"))
        (if-let [path (get-path-to-list lists list-id)]
          (do (log (str "Path: " path))
              (let [new-lists (t/dissoc-in+ lists path)]
                (log (str "New lists: " (:xs new-lists)))
                new-lists))
          (do (log (str "No list found with id: " list-id))
              lists)))))

(defn add-list [next-to-or-sub lists list-id & {:keys [content link-mode]
                                 :or {content ""
                                      link-mode nil}}]
  "Will create a new list after list (with list-id).
  Params:
      - Keyword mode: either :sibling or :sublist, depending on where to add list item."
  (do (log (str "List id: " list-id))
      (if-let [preliminary-path (get-path-to-list lists list-id)]
        (let [;;Depending on next-to-or-sub, we will add a sibling item or add sublists
              path (if (= next-to-or-sub :sibling)
                     preliminary-path
                     (-> preliminary-path (conj :xs) (conj 0)))]
          (do (log (str "Path: " path))
              (let [list-index (last path)
                    path-to-vec (butlast path) #_(t/convert-to-vec (butlast path))
                    ;;Vector vec is where list is at. vec is never nil (note: or)
                    vec (or (get-in lists path-to-vec) [])
                    new-vec (->> (make-list :content content
                                            :link-mode link-mode)
                                 (t/add-vec-elem vec list-index))
                    new-lists (do  (log "NEW LIST: "
                                        (make-list :content content
                                                   :link-mode link-mode))
                                   (log "NEW VEC " new-vec)
                                   (log "VEC " vec)
                                   (log "LISTS " lists)
                                   (log "PATH " path-to-vec)
                                   (assoc-in lists path-to-vec new-vec)) ]
                (log (str "New lists: " (:xs new-lists)))
                new-lists))
          )
        (do (log (str "No list found with id: " list-id))
            lists))))

(def add-list-sibling (partial add-list :sibling))
(def add-list-sublist (partial add-list :sublist))

(defn add-list-many [next-to-or-sub lists list-id many-items]
  "Params:
    - Vector many-items contains maps, each with keyword :content, :link-mode etc.
    - Keyword next-to-or-sub: either :sibling or :sublist, depending on where to add list item."
  (let [add-fn (if (= next-to-or-sub :sublist) add-list-sublist add-list-sibling)]
    (reduce (fn [prev-list next-item]
              (add-fn prev-list list-id
                      :content (:content next-item)
                      :link-mode (:link-mode next-item)))
            lists
            many-items)))


