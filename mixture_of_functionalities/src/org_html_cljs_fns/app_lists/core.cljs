(ns ^:figwheel-hooks org-html-cljs-fns.app-lists.core
  (:require [goog.dom :as dom]
            [goog.dom.TagName :as TagName]
            ;;[goog.events :as evts]
            [re-frame.core :as rf]
            [reagent.dom :as rdom]
            [org-html-cljs-fns.tools :as t :refer [rand-str log log-ind error alert]]
            [org-html-cljs-fns.app-lists.views :as v]
            ;; These two are only required to make the compiler load them
            ;; (see https://github.com/day8/re-frame/blob/master/docs/App-Structure.md)
            [org-html-cljs-fns.app-lists.events]
            [org-html-cljs-fns.app-lists.subs]))

;; -- Debugging aids ----------------------------------------------------------
;;(devtools/install!)       ;; we love https://github.com/binaryage/cljs-devtools
;;(enable-console-print!)   ;; so that println writes to `console.log`

;;----------------------------------------------------
;; Domino 1 - Event Dispatch
;;----------------------------------------------------


(defn init-db []
  ;;(rf/dispatch-sync [:initialize-clock])
  ;; Put an initial value into app-db.
  ;; The event handler for `:initialize` can be found in `events.cljs`
  ;; Using the sync version of dispatch means that value is in
  ;; place before we go onto the next step.
  (rf/dispatch-sync [:initialize]))


;;----------------------------------------------------
;; Render
;;----------------------------------------------------

(defn add-todo-app []
  (let [title-node (.item (dom/getElementsByTagName TagName/H1 (dom/getElement "content"))
                          0)
        app-node (dom/createElement TagName/DIV)]
    (do (.setAttribute app-node "id" "app-lists")
        (.setAttribute app-node "class" "outline-2")
        (dom/insertSiblingAfter app-node title-node)
        ;;Render appp
        (rdom/render [v/todo-app] app-node))))



(defn ^:export run []
  (init-db)
  (add-todo-app))

;;----------------------------------------------------
;; Figwheel reload hooks
;; first notify figwheel that this ns has callbacks defined in it
;; with: (ns ^:figwheel-hooks ...)
;; See: https://figwheel.org/docs/hot_reloading.html
;;---------------------------------------------------

(defn ^:before-load my-before-reload-callback []
  (log "Before reload...")
  ;;Remove app node
  #_(let [app-node (dom/getElement "app-lists")]
    (log "Removing previous app node!")
    (dom/removeNode app-node)
    ))

(defn ^:after-load my-after-reload-callback []
  (log "After reload...")
  ;; We force a UI update by clearing the Reframe subscription cache.
  (rf/clear-subscription-cache!))





;;----------------------------------------------------
;; REPL debugging
;;---------------------------------------------------

(comment

  ;;; Trying out closure events
  (defn handle-click [event] ;an event object is passed to all events
  (js/alert "button pressed"))
  (evts/listen
   (dom/getElement "postamble"); This is the dom element the event comes from
   (.-CLICK evts/EventType); This is a string or array of strings with the event names. 
   ;;All event names can be found in the EventType enum
   handle-click ;function that should handle the event
   )

  ;;
  ;; Repl workflow
  ;;
  (ns org-html-cljs-fns.app-lists)
  (load-namespace 'org-html-cljs-fns.class-utils)
  (require 'goog.dom)
  (require 'goog.events)

  (def c (goog.dom/getElement "content"))
  (def os (array-seq
               (.querySelectorAll c
                                  ;;(str "div.outline-" 3)
                                  outlines-css-selector)))
  (some #(= :status %) (classes-of (dom/getElement "postamble")))


  (ns org-html-cljs-fns.app-lists
    (:require cljsjs.react-sortable-hoc))
  )
