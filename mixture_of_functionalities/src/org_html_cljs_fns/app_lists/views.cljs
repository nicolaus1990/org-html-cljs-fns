(ns org-html-cljs-fns.app-lists.views
  (:require [clojure.set :as cljSet]
            [re-frame.core :as rf]
            [reagent.core :as r]
            [org-html-cljs-fns.app-lists.db :as db]
            [org-html-cljs-fns.app-lists.views.bootstrap-utils :as bc]
            [org-html-cljs-fns.class-utils :as ct]
            [org-html-cljs-fns.tools :as t :refer [rand-str log log-ind error alert]]
            [org-html-cljs-fns.app-lists.views.re-com-utils :as rcu]
            ;; Re-com (css)
            [re-com.core    :as rc  :refer [h-box v-box box gap line border title label
                                            single-dropdown modal-panel progress-bar
                                            input-text input-textarea label checkbox button
                                            hyperlink-href p p-span]]
            [re-com.modal-panel     :refer [modal-panel-args-desc]]
            [re-com.dropdown :refer [filter-choices-by-keyword single-dropdown-args-desc]]
            ;; Note: some cljsjs libs dont have global-exports support (ie:
            ;; :as and :refer do not work with any library). Check:
            ;; https://github.com/cljsjs/packages/tree/master/react-sortable-hoc
            [react-sortable-hoc :as sort]))

;;----------------------------------------------------
;; Reframe tools
;;----------------------------------------------------

(def <sub  (comp deref rf/subscribe))
(def >evt rf/dispatch)

;;----------------------------------------------------
;; Modal dialog panels
;;----------------------------------------------------
(defn edit-panel [id toggle-panel]
  ;;TODO: Do we have to deref (take content) and then rewrap again in atom?
  ;; Cant we just take the atom that dispatch gives back, and change that?
  ;; how will reframe behave?
  (fn []
    (let [l (<sub [:list-by-id id])
          list-content (r/atom (db/get-list-content l))
          is-link-list (r/atom (db/get-list-is-link-list l))
          process-ok (fn [event]
                       (println "Submitted")
                       (do (log "*** Edit: content: " @list-content))
                       ;;TODO: Checkbox: reframe events with multivariate args?
                       (>evt [:list-update id db/kw-content @list-content])
                       (toggle-panel)
                       ;; ***** PROCESS THE RETURNED DATA HERE
                       false) ;; Prevent default "GET" form submission (if used)
          process-cancel (fn [event]
                           (toggle-panel)
                           (println "Cancelled")
                           false)]
      [border
       :border "1px solid #eee"
       :child  [v-box
                :padding  "10px"
                :style    {:background-color "white"}
                :children [[box
                            :align-self :end
                            :size "auto"
                            :child [button :label    [:i.zmdi.zmdi-close]
                                           :on-click process-cancel]]
                           [title :label "Edit" :level :level2]
                           [v-box
                            :class    "form-group"
                            :children [[input-text
                                        :model       list-content
                                        :on-change   #(reset! list-content %)
                                        ;;:placeholder "Write here..."
                                        :class       "form-control"
                                        :attr        {:id "pf-email"}]]]
                           [checkbox :label     "Hyperlink mode (items are links)"
                                     :model     is-link-list
                                     :on-change #(reset! is-link-list (not @is-link-list))]
                           [line :color "#ddd" :style {:margin "10px 0 10px"}]
                           [h-box
                            :gap      "12px"
                            :children [[button
                                        :label    "Submit"
                                        :class    "btn-primary"
                                        :on-click process-ok]
                                       #_[button
                                          :label    "Cancel"
                                          :on-click process-cancel]]]]]])))

(defn add-item-panel [id toggle-panel]
  ;;TODO: Do we have to deref (take content) and then rewrap again in atom?
  ;; Cant we just take the atom that dispatch gives back, and change that?
  ;; how will reframe behave?
  (fn []
    (let [list-content (r/atom "")
          is-link-list (r/atom nil)
          process-ok (fn [event]
                       (println "Submitted")
                       (do (log "*** Add item: content: " @list-content))
                       (>evt [:list-add :sibling id [{:content @list-content}]])
                       (toggle-panel)
                       ;; ***** PROCESS THE RETURNED DATA HERE
                       false) ;; Prevent default "GET" form submission (if used)
          process-cancel (fn [event]
                           (toggle-panel)
                           (println "Cancelled")
                           false)]
      [border
       :border "1px solid #eee"
       :child  [v-box
                :padding  "10px"
                :style    {:background-color "white"}
                :children [[box
                            :align-self :end
                            :size "auto"
                            :child [button :label    [:i.zmdi.zmdi-close]
                                    :on-click process-cancel]]
                           [title :label "Add item" :level :level2]
                           [v-box
                            :class    "form-group"
                            :children [[input-text
                                        :model       list-content
                                        :on-change   #(reset! list-content %)
                                        :placeholder "Write here..."
                                        :class       "form-control"
                                        :attr        {:id "pf-email"}]]]
                           [checkbox :label     "Hyperlink mode"
                            :model     is-link-list
                            :on-change #(reset! is-link-list (not @is-link-list))]
                           [line :color "#ddd" :style {:margin "10px 0 10px"}]
                           [h-box
                            :gap      "12px"
                            :children [[button
                                        :label    "Submit"
                                        :class    "btn-primary"
                                        :on-click process-ok]]]]]])))

(defn add-many-items-panel [id toggle-panel]
  (fn []
    (let [text-area-content (r/atom "")
          is-link-list (r/atom nil)
          process-ok (fn [event]
                       (println "Submitted")
                       (do (log "*** Add many items: content: " @text-area-content))
                       (let [items (t/split-and-do @text-area-content
                                               (fn [s] {:content s
                                                        :link-mode @is-link-list}))]
                         (log "items: " items)
                         (>evt [:list-add :sublist id items])
                         )
                       (toggle-panel)
                       ;; ***** PROCESS THE RETURNED DATA HERE
                       false) ;; Prevent default "GET" form submission (if used)
          process-cancel (fn [event]
                           (toggle-panel)
                           (println "Cancelled")
                           false)]
      [border
       :border "1px solid #eee"
       :child  [v-box
                :padding  "10px"
                :style    {:background-color "white"}
                :children [[box
                            :align-self :end
                            :size "auto"
                            :child [button :label    [:i.zmdi.zmdi-close]
                                    :on-click process-cancel]]
                           [title :label "Add item" :level :level2]
                           [v-box
                            :class    "form-group"
                            :children [[input-textarea
                                        :model       text-area-content
                                        :on-change   #(reset! text-area-content %)
                                        :placeholder "Write here..."
                                        :class       "form-control"
                                        :attr        {:id "pf-email"}]]]
                           [checkbox :label     "Hyperlink mode"
                            :model     is-link-list
                            :on-change #(reset! is-link-list (not @is-link-list))]
                           [line :color "#ddd" :style {:margin "10px 0 10px"}]
                           [h-box
                            :gap      "12px"
                            :children [[button
                                        :label    "Submit"
                                        :class    "btn-primary"
                                        :on-click process-ok]]]]]])))


#_(defn dialog-markup
  [form-data process-ok process-cancel]
  [border
   :border "1px solid #eee"
   :child  [v-box
            :padding  "10px"
            :style    {:background-color "cornsilk"}
            :children [[title :label "Welcome to MI6. Please log in" :level :level2]
                       [v-box
                        :class    "form-group"
                        :children [[:label {:for "pf-email"} "Email address"]
                                   [input-text
                                    :model       (:email @form-data)
                                    :on-change   #(swap! form-data assoc :email %)
                                    :placeholder "Enter email"
                                    :class       "form-control"
                                    :attr        {:id "pf-email"}]]]
                       [v-box
                        :class    "form-group"
                        :children [[:label {:for "pf-password"} "Password"]
                                   [input-text
                                    :model       (:password @form-data)
                                    :on-change   #(swap! form-data assoc :password %)
                                    :placeholder "Enter password"
                                    :class       "form-control"
                                    :attr        {:id "pf-password" :type "password"}]]]
                       [checkbox
                        :label     "Forget me"
                        :model     (:remember-me @form-data)
                        :on-change #(swap! form-data assoc :remember-me %)]
                       [line :color "#ddd" :style {:margin "10px 0 10px"}]
                       [h-box
                        :gap      "12px"
                        :children [[button
                                    :label    "Sign in"
                                    :class    "btn-primary"
                                    :on-click process-ok]
                                   [button
                                    :label    "Cancel"
                                    :on-click process-cancel]]]]]])

#_(defn modal-dialog
  "Create a button to test the modal component for modal dialogs"
  []
  (let [show?       (r/atom false)
        form-data      (r/atom {:email       "james.bond.007@sis.gov.uk"
                                :password    "abc123"
                                :remember-me true})
        save-form-data (r/atom nil)
        process-ok     (fn [event]
                         (reset! show? false)
                         (println "Submitted form data: " @form-data)
                         ;; ***** PROCESS THE RETURNED DATA HERE
                         false) ;; Prevent default "GET" form submission (if used)
        process-cancel (fn [event]
                         (reset! form-data @save-form-data)
                         (reset! show? false)
                         (println "Cancelled form data: " @form-data)
                         false)]
    (fn []
      [v-box
       :children [[button
                   :label    "Modal Dialog"
                   :class    "btn-info"
                   :on-click #(do
                                (reset! save-form-data @form-data)
                                (reset! show? true))]
                  (when @show? [modal-panel
                                :backdrop-color   "grey"
                                :backdrop-opacity 0.4
                                :style            {:font-family "Consolas"}
                                :child            [dialog-markup
                                                   form-data
                                                   process-ok
                                                   process-cancel]])]])))

;;----------------------------------------------------
;; Drop down menu
;;----------------------------------------------------


#_(defn simple-demo-2
  []
  [v-box
   :gap "5%"
   :children [[p "Simple demo 2"]]])

#_(defn simple-demo
  []
  [v-box
   :gap "5%"
   :children [[p "The dropdown above is the simple case."]
              [p "It presents a list of choices and allows one to be selected, via mouse or keyboard."]]])

#_(defn item-dropdown-menu
  []
  (let [selected-demo-id (r/atom 1)]
    (fn []
      [v-box
       :size     "auto"
       :gap      "5%"                                       ;;"5px"
       :children [[v-box
                   :gap      "5%"
                   :children [[v-box
                               :gap      "10%"
                               :width    "25%"
                               :children [[p-span
                                           "A dropdown selection component, similar to "
                                           [hyperlink-href
                                            :label  "Chosen"
                                            :href   "http://harvesthq.github.io/chosen"
                                            :target "_blank"]
                                           ", styled using "
                                           [hyperlink-href
                                            :label  "Bootstrap"
                                            :href   "https://github.com/alxlit/bootstrap-chosen"
                                            :target "_blank"]
                                           "."]
                                          [p "Note: Single selection only."]
                                          ;;[args-table single-dropdown-args-desc]
                                          ]]
                              [v-box
                               :width     "20%"
                               :gap       "5%"
                               :children  [[h-box
                                            :gap      "5%"
                                            :align    :center
                                            :children [[label :label "Select a demo"]
                                                       [gap :size "5px"]
                                                       [single-dropdown
                                                        :choices   [{:id 1 :label "Simple dropdown"}
                                                                    {:id 2 :label "Display simple demo 2"}
                                                                    ]
                                                        :model     selected-demo-id
                                                        :width     "200px"
                                                        :on-change #(reset! selected-demo-id %)]]]
                                           ;;[gap :size "0px"] ;; Force a bit more space here
                                           (case @selected-demo-id
                                             1 [simple-demo]
                                             2 [simple-demo-2])]]]]]])))


;;----------------------------------------------------
;; Button icons
;;----------------------------------------------------

(defn list-item-menu [show-panel? fn-remove-list]
  [:ul
   [:li {:on-click #(reset! show-panel? :edit)}
    [:i.zmdi.zmdi-edit]
    [:span {:style {:padding-left "8px"}} "Edit"]]
   [:li {:on-click #(reset! show-panel? :add-item)}
    [:i.zmdi.zmdi-plus-circle-o]
    [:span {:style {:padding-left "8px"}} "Add new item"] ]
   [:li {:on-click #(reset! show-panel? :add-sub-item)}
    [:i.zmdi.zmdi-plus-circle-o-duplicate]
    [:span {:style {:padding-left "8px"}} "Add new sub-item" ]]
   #_[:li {:on-click #(reset! show-panel? :is-link)}       ;;See :edit panel
    ;;[:i.zmdi.zmdi-link]
    [:i.zmdi.zmdi-globe-alt]
    [:span {:style {:padding-left "8px"}} "Is link." ]]
   [:li {:on-click #(reset! show-panel? :save)}
    [:i.zmdi.zmdi-refresh-sync]
    [:span {:style {:padding-left "8px"}} "Save"] ]
   [:li {:on-click #(reset! show-panel? :settings)}
    [:i.zmdi.zmdi-settings]
    [:span {:style {:padding-left "8px"}} "Settings"] ]
   [:li {:on-click fn-remove-list #_#(reset! show-panel? :remove)}
    ;;[:i.zmdi.zmdi-minus-circle-outline]
    ;;[:i.zmdi.zmdi-delete]
    [:i.zmdi.zmdi-close]
    [:span {:style {:padding-left "8px"}} "Remove item." ]]
   ]
  )

(defn menu-down-button [id]
  (let [show-dropdowm (r/atom false)
        toggle-dropdown #(reset! show-dropdowm (not @show-dropdowm))
        ;;
        show-panel? (r/atom nil)
        toggle-panel #(reset! show-panel? (not @show-panel?))]
    (fn []
      [:span ;[:span.menuDownIcon [bc/drop-down-menu-icon]]
       [:div.dropdown                                          ;;{:on-click #(js/alert "HEY!")}
        [:span.menuDownIcon                                    ;;{:on-click #(js/alert "HEY!")}
         {:on-click #(do (log "SHOW: " @show-dropdowm)
                         (reset! show-dropdowm (not @show-dropdowm))
                         (log "SHOW AFTER; " @show-dropdowm)
                         (log "ID:  " id))}
         ;;[:i.zmdi.zmdi-menu ]
         [:i.zmdi.zmdi-more-vert]
         ]
        (when @show-dropdowm
          [:div.dropdown-content {:on-click toggle-dropdown}
           [list-item-menu show-panel? #(>evt [:list-remove id])]
           ])
        ]
       (when (and @show-panel?
                  (not (= :remove @show-panel?)))
         [:span
          [modal-panel
           :backdrop-color   "grey"
           :backdrop-opacity 0.4
           ;;:style            {:font-family "Consolas"}
           :child
           (do (log "Show panel: " @show-panel?)
               (case @show-panel?
                 :edit [edit-panel id toggle-panel]
                 :add-item [add-item-panel id toggle-panel]
                 :add-sub-item [add-many-items-panel id toggle-panel] #_[add-items-panel id toggle-panel]
                 :save [:span] #_[save-panel id toggle-panel]
                 :settings [:span] #_[settings-panel id toggle-panel]
                 :remove [:span] #_[add-items-panel id toggle-panel]
                 ))
           ]]
         )
       ]
      )
    ))

(defn move-button []
  ;[:span.moveElemIcon [bc/move-elem-icon]]
  [:span.moveElemIcon [:i.zmdi.zmdi-arrows]]
  )


;;------------------------------------------------------
;; Sortable list.
;; Example taken from:
;; https://github.com/reagent-project/reagent/blob/master/examples/react-sortable-hoc/src/example/core.cljs
;;------------------------------------------------------

(def DragHandle
  (sort/SortableHandle.
    ;; Alternative to r/reactify-component, which doens't convert props and hiccup,
    ;; is to just provide fn as component and use as-element or create-element
    ;; to return React elements from the component.
    (fn []
      ;; content of span.grippy is going to be overwritten by css (pretty grip handler).
      (r/as-element                                         ;;[:span.grippy "::"]
        [:span.grippy [move-button]]))))

;; Mutually recursive function: sortable-component SortableItem
(declare sortable-component)

(def SortableItem
  (sort/SortableElement.
    (r/reactify-component
      (fn [{:keys [id content subList]}]
        [:li
         [:span.boundBox [:> DragHandle] [menu-down-button id] content]
         ;;(let [list-obj (<sub [:list-obj id])])
         (if (not (empty? subList))
           [:span.subList [sortable-component id]]
           [:span.emptyList])]))))

;; Alternative without reactify-component
;; props is JS object here
#_
    (def SortableItem
      (sort/SortableElement.
        (fn [props]
          (r/as-element
            [:li
             [:> DragHandle]
             (.-value props)]))))

(def SortableList
  (sort/SortableContainer.
    (r/reactify-component
      (fn [{:keys [items]}]
        [:ul.sortableList
         (for [[item index] (map vector items (range))]
           ;; No :> or adapt-react-class here because that would convert value to JS
           (let [id (:id item)
                 content (db/get-list-content item)
                 subList (db/get-list-items item)]
             (r/create-element
              SortableItem
              #js {:key (str "item-" index "-" id)
                   :id id
                   :index index
                   :content content
                   :subList subList })))]))))

;; Or using new :r> shortcut, which doesn't do props conversion
#_
    (def SortableList
      (sort/SortableContainer.
        (r/reactify-component
          (fn [{:keys [items]}]
            [:ul
             (for [[value index] (map vector items (range))]
               [:r> SortableItem
                #js {:key (str "item-" index)
                     :index index
                     :value value}])]))))



(defn sortable-component [list-id]
  (let [l-obj (<sub [:list-by-id list-id])]
    ;;(<sub [:list-obj "root"]) should be something like: {:x "..." xs: [...]} ,previously: (r/atom (get-app-list-state))
    (r/create-element
     SortableList
     #js {:items     (db/get-list-items l-obj)
          :onSortEnd (fn [event] (>evt [:list-item-drag event list-id]))
          :useDragHandle true})))

(defn sortable-list []
  (let [list-id (<sub [:list-id-to-display])]
    (log "********** LIST ID: ********* " list-id)
    [sortable-component list-id]))
;; END of example


;;----------------------------------------------------
;; App window
;;----------------------------------------------------

(defn root-menu-button []
  (let [selected-item (r/atom 1)]
    (fn []
      [:span.root-menu-button
       [single-dropdown
        :width     "auto"
        :choices   [{:id 1 :label "Select something"}
                    {:id 2 :label "Display simple demo 2"}
                    {:id 3 :label "Display simple demo 3"}]
        ;:model     selected-item     ; the id of a choice
        :model nil ; if model is nil, then placeholder is taken
        :placeholder "..."
        ;;:render-fn (fn [choice] [:span.zmdi.zmdi-flower-alt])
        :on-change #(log (str "root-menu-button: Selected id: " %))]])))


(defn button-select-list []
  "Returns a button to select the list.
  If optional :preprocess-choices arg is passed, then argument choices is assumed to be a vector of lists and
   a proper "
  (fn []

    (let [id-of-list-to-display (<sub [:list-id-to-display])
          ids-first-list (map :id (<sub [:first-level-lists]))
          ;;selected (<sub [:list-id-to-display])
          ;;Vector choices should be something like:
          ;; [{:id 1 :label \"List 1\"}
          ;;  {:id 2 :label \"List 2\"}
          ;;  {:id 3 :label \"List 3\"}]
          choices (-> (comp #(cljSet/rename-keys % {db/kw-content :label
                                                    db/kw-id :id})
                            #(select-keys % [db/kw-id db/kw-content])
                            #(<sub [:list-by-id %]))
                      (map ids-first-list)
                      (conj {:id db/id-root-list-node :label "All lists"}))]
      (log "---------- " choices)
      (rcu/simple-dd-select-button
        (fn []
          [:h3 (-> (filter #(= id-of-list-to-display (db/kw-id %)) choices)
                   first
                   (:label)
                   (or "Unrecognized list! (error)"))
           #_(->> id-of-list-to-display
                    (db/get-list-by-id ids-first-list)
                    (db/get-list-content))])
        choices
        (fn [id] (do (log "root-menu-button")
                     (log "     Selected id: " id)
                     (>evt [:list-to-display id])))
        ;;:model selected
        ))


    )
  )





(defn todo-app [props]
  (fn []
    [:div
     [:h2 "App"]
     #_[:div.outline-3
      #_[modal-dialog]
      [item-dropdown-menu]]
     [:div.outline-3
      [button-select-list]
      [sortable-list]
      #_[sortable-component]
      #_[:> js/SortableHOC.sortableContainer {:onSortEnd onSortEnd}]
      #_[:> rsjs/ReactSortable
         [:div {:key 1}
          "One item over here"]]
      ]]))
