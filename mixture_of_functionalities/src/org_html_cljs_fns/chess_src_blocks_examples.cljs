(ns org-html-cljs-fns.chess-src-blocks-examples
    (:require [clojure.string :as cljStr]
              [clojure.set :as cset]
              [goog.dom :as dom]
              [goog.dom.TagName :as TagName]
              [org-html-cljs-fns.tools :as t :refer [log log-ind error alert]]
              ;;chess.js
              [npm.chessjs]
              ;;[cljsjs.chess.js]
              [npm.chessboardjs])
    ;;(:import ["@chrisoakman/chessboardjs/dist/chessboard-1.0.0.js" Chessboard])
    )

(def example-5001-node-id "play-with-random")
(def board-5001 (atom nil))
(def engine-5001 (atom (js/Chess)))

(defn example-5001 []
  "Example: Play Random Computer  INCOMPLETE!

Requires that html has following node:
    <div id=\"play-with-random\" style=\"width: 400px\"></div>

This tries to emulate the example found at https://chessboardjs.com/examples#5001."
  (letfn [(onDragStart [src p pos ori] true)
          (make-random-move []
            (let [move (first (.moves @engine-5001))]
              ;;TODO: pick random move
              (.position @board-5001
                         (.fen @engine-5001 (.move @engine-5001 move)))))
          (onDrop [src dest]
            (do (.setTimeout js/window make-random-move 250)))
          (onSnapEnd []
            (.position @board-5001 (.fen @engine-5001)))]
    (reset! board-5001  (js/Chessboard example-5001-node-id
                                      #js {:position "start"
                                           :draggable true
                                           :onDragStart onDragStart
                                           :onDrop onDrop
                                           :onSnapEnd onSnapEnd }))))


(comment "
// NOTE: this example uses the chess.js library:
// https://github.com/jhlywa/chess.js

var board = null
var game = new Chess()

function onDragStart (source, piece, position, orientation) {
  // do not pick up pieces if the game is over
  if (game.game_over()) return false

  // only pick up pieces for White
  if (piece.search(/^b/) !== -1) return false
}

function makeRandomMove () {
  var possibleMoves = game.moves()

  // game over
  if (possibleMoves.length === 0) return

  var randomIdx = Math.floor(Math.random() * possibleMoves.length)
  game.move(possibleMoves[randomIdx])
  board.position(game.fen())
}

function onDrop (source, target) {
  // see if the move is legal
  var move = game.move({
    from: source,
    to: target,
    promotion: 'q' // NOTE: always promote to a queen for example simplicity
  })

  // illegal move
  if (move === null) return 'snapback'

  // make random legal move for black
  window.setTimeout(makeRandomMove, 250)
}

// update the board position after the piece snap
// for castling, en passant, pawn promotion
function onSnapEnd () {
  board.position(game.fen())
}

var config = {
  draggable: true,
  position: 'start',
  onDragStart: onDragStart,
  onDrop: onDrop,
  onSnapEnd: onSnapEnd
}
board = Chessboard('myBoard', config)

")


(defn run []
  (example-5001))



