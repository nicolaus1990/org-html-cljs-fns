(ns user
  (:require [figwheel.server.ring]
            [figwheel.main.schema.config]
            [figwheel.main.api :as api]
            [clojure.string :as string]))

(defn strip-leading-whitspace [s]
  (string/join "\n"
               (map
                 #(string/replace % #"^\s\s(.*)" "$1")
                 (string/split-lines s))))

(def doc-string #(-> % resolve meta :doc strip-leading-whitspace))
(def args #(-> % resolve meta :arglists))

(defn sym->markdown-doc [sym]
  (str
    "## `" sym "`\n\n"
    "Args: `"(pr-str (args sym)) "`\n\n"
    (doc-string sym) "\n\n"))

(defn api-docs []
  (let [syms ['figwheel.main.api/start
              'figwheel.main.api/cljs-repl
              'figwheel.main.api/repl-env
              'figwheel.main.api/stop
              'figwheel.main.api/stop-all
              'figwheel.main.api/start-join]]
    (string/join "\n" (map sym->markdown-doc syms))))

(defn build-api-docs []
  (spit "docs/_includes/main-api-docs.md"
        (str "<!-- DO NOT EDIT: File generated from defn string docs in src/figwheel/main/api.clj -->\n"
             "<!-- Generation code can be found in user.clj -->\n"
             (api-docs))))

(defn build-option-docs []
  (doseq [dir ["doc/figwheel-main-options.md" "docs/config-options.md"]]
    (figwheel.main.schema.core/output-docs dir)))

(defn build-docs []
  (build-api-docs)
  (build-option-docs))







;;------------------------------------------------
;; Old figwheel config with lein-figwheel plugin and figwheel-sidecar dependency.
;;------------------------------------------------


(comment

  (ns user
    (:require
      [figwheel-sidecar.repl-api :as f]))

  ;; user is a namespace that the Clojure runtime looks for and
  ;; loads if its available

  ;; You can place helper functions in here. This is great for starting
  ;; and stopping your webserver and other development services

  ;; The definitions in here will be available if you run "lein repl" or launch a
  ;; Clojure repl some other way

  ;; You have to ensure that the libraries you :require are listed in your dependencies

  ;; Once you start down this path
  ;; you will probably want to look at
  ;; tools.namespace https://github.com/clojure/tools.namespace
  ;; and Component https://github.com/stuartsierra/component


  (defn fig-start
    "This starts the figwheel server and watch based auto-compiler."
    []
    ;; this call will only work as long as your :cljsbuild and
    ;; :figwheel configurations are at the top level of your project.clj
    ;; and are not spread across different lein profiles

    ;; otherwise you can pass a configuration into start-figwheel! manually
    (f/start-figwheel!))

  (defn fig-stop
    "Stop the figwheel server and watch based auto-compiler."
    []
    (f/stop-figwheel!))

  ;; if you are in an nREPL environment you will need to make sure you
  ;; have setup piggieback for this to work
  (defn cljs-repl
    "Launch a ClojureScript REPL that is connected to your build and host environment."
    []
    (f/cljs-repl))
  )
