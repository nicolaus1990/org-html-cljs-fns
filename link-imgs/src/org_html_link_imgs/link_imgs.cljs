(ns org-html-link-imgs.link-imgs
  (:require [clojure.string :as cljStr]
            [clojure.set :as cljSet]
            [goog.dom :as dom]
            [goog.dom.TagName :as TagName]
            ;;[goog.events :as evts]
            [org-html-link-imgs.class-utils :as ct]
            [org-html-link-imgs.tools :as t :refer [log log-ind error alert]]))


;;----------------------------------------------------
;; 
;;----------------------------------------------------

(def regex-link-with-text #"//.+\.(com|org|net|io|info|int|edu|gov|de)(/|$).*")
(def regex-link-only      #"//.+\.(com|org|net|io|info|int|edu|gov|de)(/|$)")

(defn get-img-src [a-href]
  ;;if match: should return something like "https://icons.duckduckgo.com/ip3/www.google.com.ico"
  (and (re-seq regex-link-with-text a-href)
       (let [src 
             (-> (first (re-find regex-link-only  a-href))
                 ;;result up until here is something like "//google.com" or "//google.com/"
                 ;; Remove slashes (first ones and the last one):
                 (clojure.string/replace #"^//" "")
                 (clojure.string/replace #"/$" ""))]
         (str "https://icons.duckduckgo.com/ip3/" src ".ico"))))


(defn add-img-tags [root-node]
  ;; <img height="16" width="16" src='https://icons.duckduckgo.com/ip3/www.google.com.ico' />
  (let [;; Can't just take any link, since there are links in table-of-contents (which is inside content)
        ;;(dom/getElementsByTagName (.-A dom/TagName) (dom/getElement "content"))
        ;; ass := List of html collections
        ;; as := html collection
        ;; a := link (html a-tag)
        ass (map #(dom/getElementsByTagName TagName/A %)
                 (array-seq (dom/getElementsByClass "outline-2")))]
    (doseq [as ass]
      (doseq [a (array-seq as)]
        ;;(print (str "**** TRYING: " (.-nodeType a) (.-textContent a) (.getAttribute a "href")))
        (if-let [img-src (and (.hasAttribute a "href") ;;ignore inner or broken links
                              (get-img-src (.getAttribute a "href")))]
          (let [img-node (dom/createElement TagName/IMG)]
            (do (.setAttribute img-node "height" "20")
                (.setAttribute img-node "width" "20")
                (.setAttribute img-node "src" img-src)
                ;; For readthedocs css, add this css snippet:
                ;; #content img.imgWebIcon {max-width: 20px;}
                (.setAttribute img-node "class" "imgWebIcon")
                (set! (.. img-node -style -margin) "5px 5px 5px 5px")
                ;;(.setAttribute img-node "src" "https://icons.duckduckgo.com/ip3/www.google.com.ico")
                (dom/insertSiblingBefore img-node a))))))))


(defn run []
  (let [c (dom/getElement "content")]
    (add-img-tags c)
    ))

;;----------------------------------------------------
;; Debugging
;;----------------------------------------------------


(comment
  ;;
  ;; Repl workflow
  ;;
  (ns org-html-link-imgs.hide-outlines)
  (load-namespace 'org-html-link-imgs.class-utils)
  (require 'goog.dom)
  (require 'goog.events)

  (def c (goog.dom/getElement "content"))
  (def os (array-seq
               (.querySelectorAll c
                                  ;;(str "div.outline-" 3)
                                  outlines-css-selector)))


  (goog.dom/getElement "outline-container-org8e9c312")

  ;;; Trying out closure events
  (defn handle-click [event] ;an event object is passed to all events
  (js/alert "button pressed"))
  (evts/listen
   (dom/getElement "postamble"); This is the dom element the event comes from
   (.-CLICK evts/EventType); This is a string or array of strings with the event names. 
   ;;All event names can be found in the EventType enum
   handle-click ;function that should handle the event
   )



  (let [link-nodes (.item (goog.dom/getElementsByTagName (.-A goog.dom/TagName) (goog.dom/getElement "content"))
                          0)
        ]
    (doseq [a (array-seq link-nodes)]
      (let [img-node (goog.dom/createElement (.-IMG goog.dom/TagName))]
        (do (.setAttribute img-node "height" "16")
            (.setAttribute img-node "width" "16")
            (.setAttribute img-node "src" "https://icons.duckduckgo.com/ip3/www.google.com.ico")
            (goog.dom/insertSiblingAfter img-node a)))))


  
  )
