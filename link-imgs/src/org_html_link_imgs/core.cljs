(ns ^:figwheel-hooks org-html-link-imgs.core
  (:require [org-html-link-imgs.tools :as t :refer [rand-str log log-ind error alert]]
            [org-html-link-imgs.search-bar :refer [run-injectSearchBar]]
            [org-html-link-imgs.hide-outlines :as ho :refer [run]]
            [org-html-link-imgs.config :as config]
            [org-html-link-imgs.link-imgs :as li]))

;; -- Debugging aids ----------------------------------------------------------
;;(devtools/install!)       ;; we love https://github.com/binaryage/cljs-devtools
;;(enable-console-print!)
;; See org-html-link-imgs.config

(println "This text is printed from src/org-html-link-imgs/core.cljs. Edit it and see reloading in action!")

(defn dev-setup []
  "Function to setup whatever it needs for development"
  (when config/debug?
    (println "Development mode is activated!")
    ;;(devtools/install!)
    (do
      (js/console.log "Trying out if npm external (non-cljsjs, non-closure  modules) deps are loaded. ")
      ;; moment.js
      #_(do  (js/console.log "Testing moment library.")
           (js/console.log  (str "Requires: moment.js (\"$ npm add moment\" and "
                                 "in this file \"(:require [moment])\")."))
           (js/console.log moment)
           (js/console.log (str "Hello there it's moment.js loaded with npm and webpack. Time of day: "
                                (.format (moment) "dddd"))))
      )))


(defn ^:export start-up []
  "This start-up fn has meta-data :export, which means, can be run in html (js) with: 
<script>org-html-link-imgs.core.start_up();</script>"
  (dev-setup)
  #_(run-injectSearchBar)
  #_(ho/run)
  (li/run))

(js/setTimeout start-up 1000)




;;----------------------------------------------------
;; Figwheel reload hooks
;; first notify figwheel that this ns has callbacks defined in it
;; with: (ns ^:figwheel-hooks ...)
;; See: https://figwheel.org/docs/hot_reloading.html
;;---------------------------------------------------

(defn ^:before-load my-before-reload-callback []
  (log (str "You can actually have :figwheel-hooks individually in each file. Here "
            "this function has to call the before-load fns of each namespaces."))
  (log "Before reload...")
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
     )

(defn ^:after-load my-after-reload-callback []
  (log "After reload...")
  ;; We force a UI update by clearing the Reframe subscription cache.
  ;;(rf/clear-subscription-cache!)
  )



