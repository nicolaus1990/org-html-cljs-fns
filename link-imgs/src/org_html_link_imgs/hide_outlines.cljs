(ns org-html-link-imgs.hide-outlines
  (:require [clojure.string :as cljStr]
            [clojure.set :as cljSet]
            ;; Doc of closure lib:
            ;;        https://google.github.io/closure-library/
            ;;        https://google.github.io/closure-library/api/goog.dom.html
            [goog.dom :as dom]
            [goog.events :as evts]
            ;;[goog.events.EventType :refer [CLICK]];; Works, but better: see :import
            [org-html-link-imgs.class-utils :as ct]
            [org-html-link-imgs.tools :as t :refer [log log-ind error alert]])
  ;; In :import we import classes and enums (and only libs that are compatible
  ;; with Google's Closure library).
  ;; - EventType is Enum
  (:import [goog.events EventType]))



(def outlines ["outline-2" "outline-3" "outline-4" "outline-5" "outline-6"])
(def outlines-css-selector
  (reduce (fn [s s2] (str s ", " s2))
          (map #(str "div." %) outlines)))


(defn node-header? [node]
  (some #(= (.-tagName node) %) ["H1" "H2" "H3" "H4" "H5" "H6"])
  #_(or (= (.-tagName node) "H1")
      (= (.-tagName node) "H2")
      (= (.-tagName node) "H3")
      (= (.-tagName node) "H4")
      (= (.-tagName node) "H5")
      (= (.-tagName node) "H6")
      ))

#_(defn getFirstNodeIfHeading [node]
  (let [child-node (dom/getFirstElementChild node)]
    (if (node-header? child-node) child-node nil)))


(defn toggle-hide! [node] 
  ;; Requires in css at least: .hide { display: none;}
  (ct/toggle-class! node "hide"))

(defn hide-children! [div]
  ;; Param div should be an div.outline node.
  ;; Hiddes all children that are not headers (actually: toggles)
  (doseq [child (array-seq (.-children div))]
    (when (not (node-header? child))
      (toggle-hide! child))))


(defn add-hide-click-evts [root-node]
  ;; Requires in css at least: .hide { display: none;}
  (let [nodes (array-seq
               (.querySelectorAll root-node
                                  ;;(str "div.outline-" 3)
                                  outlines-css-selector))]
    (doseq [div nodes]
      (let [header (dom/getFirstElementChild div)]
        (when (node-header? header)
          (evts/listen header
                       EventType.CLICK
                       (fn [e]
                         (do (print "Header clicked!")
                             #_(do ;;Debugging prints
                                 (print div)
                                 (print (.-textContent div))
                                 (print (first (array-seq (.-children div))))
                                 (print (.-textContent (goog.dom/getFirstElementChild (.item (.-children div) 0))))
                                 (print "CHILDREN:")
                                 (doseq [child (array-seq (.-children div))] (print child))
                                 (print "END!"))
                             (hide-children! div)))))))))


#_(defn hide-tags
  ;; Param tag is of type: goog.dom/TagName.<?>
  ;; Default: If no tag is given: Will hide H2 tags
  ;; Requires in css at least: .hide { display: none;}
  ([root-node] (hide-tags root-node (.-H2 dom/TagName)))
  ([root-node tag]
   (let [tags (dom/getElementsByTagName tag root-node)]
     (doseq [t (array-seq tags)]
       (toggle-hide! t)))))

(defn hide-by-class [root-node class]
  ;; Requires in css at least: .hide { display: none;}
  (let [nodes (dom/getElementsByClass class root-node)]
    (doseq [node (array-seq nodes)]
      (hide-children! node))))

(defn show-all [root-node]
  ;; Requires in css at least: .hide { display: none;}
  (let [nodes (dom/getElementsByClass "hide" root-node)]
    (doseq [node (array-seq nodes)]
      (toggle-hide! node))))

#_(defn add-hide-by-key-evts [root-node]
  ;;TODO: Hide outlines with keys 1,2,3,etc, classes: outline-<NUM>
  (doseq [key ["2" "3" "4" "5" "6"]]
    (evts/listen root-node
                 (.-KEYPRESS evts/EventType)
                 ;;For keycodes: https://keycode.info/
                 (fn [e] (do (print e) (if (= 49 (.-keycode e))
                                         (print "HELLO!") (print "Nope.")))))))





;;----------------------------------------------------
;; Run
;;----------------------------------------------------


(defn run []
  (let [c (dom/getElement "content")
        #_(-> js/document
              (.getElementById "content"))]
    (hide-by-class c "outline-4")
    (add-hide-click-evts c)
    #_(add-hide-by-key-evts c)
    #_(hide-tags c (.-H4 goog.dom/TagName))))


;;----------------------------------------------------
;; REPL debugging
;;---------------------------------------------------



(comment

  ;;; Trying out closure events
  (defn handle-click [event] ;an event object is passed to all events
  (js/alert "button pressed"))
  (evts/listen
   (dom/getElement "postamble"); This is the dom element the event comes from
   (.-CLICK evts/EventType); This is a string or array of strings with the event names. 
   ;;All event names can be found in the EventType enum
   handle-click ;function that should handle the event
   )

  ;;
  ;; Repl workflow
  ;;
  (ns org-html-link-imgs.hide-outlines)
  (load-namespace 'org-html-link-imgs.class-utils)
  (require 'goog.dom)
  (require 'goog.events)

  (def c (goog.dom/getElement "content"))
  (def os (array-seq
               (.querySelectorAll c
                                  ;;(str "div.outline-" 3)
                                  outlines-css-selector)))
  (some #(= :status %) (classes-of (dom/getElement "postamble")))




  (for [div os]
    #_(let [parent (goog.dom/getParentElement(goog.dom/getFirstElementChild div))])
    (let [header (goog.dom/getFirstElementChild div)]
      (when (node-header? header)
        (goog.events/listen header (.-CLICK goog.events/EventType)
                            (let [children (array-seq (.-children div))]
                              (fn [e]
                                (do (print "Header clicked!")
                                    #_(do ;;Debugging prints
                                      (print div)
                                      (print (.-textContent div))
                                      (print (first (array-seq (.-children div))))
                                      (print (.-textContent (goog.dom/getFirstElementChild (.item (.-children div) 0))))
                                      (print "CHILDREN:")
                                      (doseq [child (array-seq (.-children div))] (print child))
                                      (print "END!")
                                      (doseq [child children] (print child)))
                                    (doseq [child (array-seq (.-children div))]
                                        (when (not (node-header? child))
                                          (toggle-class! child "hide"))))))))))
  
  (for [div os]
    (let [parent (goog.dom/getParentElement(goog.dom/getFirstElementChild div))]
      (print (str "******"  parent "********\n\n"))
      (for [child (array-seq (.-children parent))]
        (when (not (node-header? child))
          (print (.-textContent child))))))


  (goog.dom/getElement "outline-container-org8e9c312")


  (toggle-class! (dom/getElement "postamble") "status" "hide")

  
  (.-tagName(goog.dom/getFirstElementChild (first os)))

  (getFirstNodeIfHeading os)
  (for [div os]
    (if-let [hChild (getFirstNodeIfHeading div)]
      (js/log hChild)))

  (for [div os]
    (if-let [hChild (getFirstNodeIfHeading div)]
      (goog.events/listen
       hChild
       (.-CLICK goog.events/EventType)
       handle-click
       )))

  (for [div os]
    (if-let [hChild (getFirstNodeIfHeading div)]
      hChild))


  
  (for [div os]
    (if-let [hChild (getFirstNodeIfHeading div)]
      (evts/listen
       hChild
       (.-CLICK evts/EventType)
       (let [css-display (atom "none")]
         (fn [event]
           (let [divChilds (filter (partial not node-header?) (dom/getChildren div))
                 toggle-val (if (= @css-display "none") "block" "none")]
             (for [divChild divChilds]
               (do
                 (reset! css-display toggle-val)
                 (set! (.. divChild -style -display) toggle-val)
                 )))
           ))
       )))



;; (if-let [hChild (goog.dom/getFirstElementChild
;;                  (nth (array-seq
;;                        (goog.dom/getElementsByClass outlines-css-selector
;;                                                     (goog.dom/getElement "content")))
;;                       1))]
;;   hChild)





;; (goog.dom/getElementsByClass outlines-css-selector
;;                              (goog.dom/getElement "content"))

;; (.-textContent (nth (array-seq (goog.dom/getElementsByClass outlines-css-selector
;;                                                             (goog.dom/getElement "content")))
;;                     17))




  
  )
