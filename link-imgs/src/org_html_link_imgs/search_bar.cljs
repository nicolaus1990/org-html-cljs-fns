(ns org-html-link-imgs.search-bar
    (:require [clojure.string :as cljStr]
              [clojure.set :as cljSet]
              [org-html-link-imgs.tools :as t :refer [log log-ind error alert]]))



;; Remember: HTMLCollections are not sequable. Need to convert to
;; array.
;; (extend-type js/HTMLCollection ;js/NodeList
;;   ISeqable
;;   (-seq [array] (array-seq array 0)))


;;-----------------------------------------------
;; Inject search bar
;;-----------------------------------------------


(defonce hiddenElemIds (atom (set [])))
(defn modifyHiddenElemIds 
  ([action]
   (modifyHiddenElemIds :none action))
  ([id action]
   (case action 
     :add (swap! hiddenElemIds conj id)
     :remove (swap! hiddenElemIds disj id)
     :reset (reset! hiddenElemIds (set []))
     (error "Wrong use of modifyId. Action: " (str action)))))

(defn showElem [node]
  (do
    (set! (.. node -style -display) "")
    (modifyHiddenElemIds (.-id node) :remove)))
(defn hideElem [node]
  (do
    (set! (.. node -style -display) "none")
    (modifyHiddenElemIds (.-id node) :add)))
(defn showAllElems []
  (doseq [id @hiddenElemIds]
    (let [node (.getElementById js/document id)]
      (showElem node)
      (modifyHiddenElemIds id :remove))))


(defn searchByTitle 
  ;;Black box: Given a keyword, recursively find headers that contain
  ;;that keyword (initial node has 'content' as id).
  ;;Inside: Function is non-tail recursive, which main input is a
  ;;node. It produces its side-effects and returns a list containing
  ;;booleans. If there is a 'true' boolean, then the given node has
  ;;header or subheaders with given keyword. 
  ;;Org's exported html files look like this: 
  ;;<div id='content'> 
  ;; <div class='outline-2'> 
  ;;  <h2>Some header </h2>
  ;;  <div class='outline-3'>
  ;;   <h3>Another header<h3> [...]
  ([title] 
   (do (log "Searching by title: " title "...")
       (searchByTitle 
        (array-seq
         (.querySelectorAll js/document 
                            (str "#content div.outline-" 2)))
        2 title)))
  ([nodes level title]
   (log-ind level nodes)
   (if (empty? nodes) false
       ;;Fn 'any-true?' is used instead of using 'some', since one
       ;;needs to iterate anyways for side-effect.
       (t/any-true?
        (doall ;Need to force lazy-sequence
         (for [div nodes] ;Use 'for' instead of 'doseq' because 'doseq' always returns nil.
           (let [heading (first (array-seq
                                 (.getElementsByTagName 
                                  div
                                  (str "h" level))))
                 t (clojure.string/lower-case (.-textContent 
                                               heading))]
             (log-ind level "Comparing: " t " with " title)
             (if (clojure.string/includes? t title)
               (do 
                 (log-ind level
                          "Match! Don't hide node (nor sub-nodes, nor parent-nodes). id: "
                          (.-id div))
                 ;;Return true to signal (in case it was part of a
                 ;;recurisve-call) that a node matches the search.
                 true)
               ;;Hide node if none of the subnodes has heading that
               ;;matches title.
               (let 
                   [subDivs (.querySelectorAll div 
                                               (str "div.outline-" (inc level)))
                    returnValsOfSearch                      
                    (searchByTitle (array-seq subDivs) (inc level) title)]
                 (if returnValsOfSearch 
                   (do (log-ind level "Some val returned true: " returnValsOfSearch)
                       true)
                   (do 
                     (log-ind level "Hidding!")
                     (hideElem div) ;; Same thing as: (hideElem (.-parentNode heading)) 
                     false)))))))))))


(defn searchByTags
  ;; Assumes input to be of form: "t:<string>"
  ;; Hides nodes that don't contain any of the given tags.
  ;; 
  ;;Org's exported html files look like this: 
  ;;<div id='content'> 
  ;; <div class='outline-2'> 
  ;;  <h2>Some header 
  ;;   <span class='tag'>
  ;;    <span class='nameOfTag'> nameOfTag </span>
  ;;   </span>
  ;;  </h2>
  ;;  <div class='outline-3'>
  ;;   <h3>Another header<h3> [...]
  ([rawTags] 
   (let [tags (set (rest (clojure.string/split rawTags #":")))
         rootNodeInList (array-seq
                   (.querySelectorAll js/document 
                                      "#content div.outline-2"))]
     (log "Searching tags: " tags)
     (searchByTags rootNodeInList 2 tags)))
  ([nodes level tags]
   (log-ind level nodes)
   (if (empty? nodes) false
       (t/any-true? 
        (doall
         (for [div nodes]
           (let [heading (first (array-seq
                                 (.getElementsByTagName 
                                  div
                                  (str "h" level))))
                 tagNodes 
                 (.querySelectorAll 
                  heading 
                  "span.tag span")
                 orgTags (set (map #(or (.-textContent %)
                                      (.-innerText %)
                                      (.-className %))
                                 (array-seq tagNodes)))]
             (if (not-empty (cljSet/intersection tags orgTags))
               (do (log-ind level "Match! id:" (.-id div))
                   true)
               (let 
                   [subDivs (.querySelectorAll div 
                                               (str "div.outline-" (inc level)))
                    returnValsOfSearch
                    (searchByTags (array-seq subDivs) (inc level) tags)]
                 (if returnValsOfSearch 
                   (do (log-ind level "Some val returned true: " returnValsOfSearch)
                       true)
                   (do 
                     (log-ind level "Hidding!")
                     (hideElem div)
                     false))))
)))))))

(defn search []
  (let [input (.getElementById js/document "myInput")
        filter (.. input -value toLowerCase)]
    (if (= filter "")
      ;;For efficiency: Don't go over all nodes just for empty str
      (do (log "Empty string. Show all nodes!")
          (showAllElems))
      (do (showAllElems)
          ;;After showing all nodes, start one of two searches:
          (if (clojure.string/starts-with? filter "t:")
            (searchByTags filter)
            (searchByTitle filter))))))

(defn injectSearchBar [siblingNode]
  (if siblingNode
    (let [div (-> js/document 
                  (.createElement  "div"))
          inp (-> js/document 
                  (.createElement  "input"))
          inpFn (fn [event] 
                  (when (= (.-keyCode event) 13) 
                    (.preventDefault event) 
                    (search))
                    ;(error "No keycode 13?")
)
          ]
      (do (.setAttribute div "id" "searchBar")
          (.setAttribute inp "id" "myInput")
          (.setAttribute inp "type" "text")
          (.setAttribute inp "placeholder" 
                         "Search for links (for tag prepend 't:' and separate with ':').")
          (.addEventListener inp "keyup" inpFn)
          (.appendChild div inp)
          ;; In js: siblingNode.parentNode.insertBefore(div, siblingNode.nextSibling);
          (.. siblingNode -parentNode 
              (insertBefore div 
                            (.-nextSibling  siblingNode)))))
    (error "No node given to insert search bar. Node has to have class name 'title'.")))

(defn run-injectSearchBar []
  (injectSearchBar 
   (-> js/document 
       (.getElementsByClassName "title")
       (.item 0))))
